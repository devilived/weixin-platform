#weixin-platform工程
------------------
准备做微信公众平台的各种接口、可以多微信号配置
------------------
多配置调用时注意、不然获取的微信配置是你的默认配置：

1. 接口调用前调用：BaseService.setThreadLocalWeixinConfig
2. 接口调用全部完成后调用：BaseService.removeThreadLocalWeixinConfig

#weixin工程
------------------
准备做成微信平台后台管理系统

1. 基于我的另一个项目：[security-manager-platform](https://git.oschina.net/devilived/security-manager-platform)；
2. 使用了开源项目[uploader](https://github.com/danielm/uploader)做图片上传；