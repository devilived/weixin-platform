package com.dev.weixin.manager.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 文本模板
 * @author 飘渺青衣
 * @see
 */
@Entity
@Table(name = "t_cdw_text")
public class TextTemplate {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="id", length = 100)
	private String id;
	
	@Column(name="name", length = 20, nullable = false)
	private String name;
	
	@Column(name="content", length = 2000, nullable = false)
	private String content;
	
	@Column(name="user_id", length = 100, nullable = false)
	private String user_id;
	
	@Column(name="createtime", length = 20, nullable = false)
	private String createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
}
