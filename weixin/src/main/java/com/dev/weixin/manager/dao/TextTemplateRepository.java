package com.dev.weixin.manager.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.dev.security.config.dao.BaseRepository;
import com.dev.weixin.manager.beans.TextTemplate;

/**
 * @author 飘渺青衣
 * @see
 */
public interface TextTemplateRepository extends BaseRepository<TextTemplate, String> {

	@Modifying
	@Query("delete from TextTemplate where id in ?1 and user_id=?2")
	public int delete(String[] ids, String user_id);
	
	@Query("select tt from TextTemplate tt where id=?1 and user_id=?2")
	public TextTemplate findOne(String id, String user_id);
}
