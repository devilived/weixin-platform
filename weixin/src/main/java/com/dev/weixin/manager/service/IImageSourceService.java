package com.dev.weixin.manager.service;

import org.springframework.web.multipart.MultipartFile;

import com.dev.security.util.DataGrid;
import com.dev.weixin.manager.beans.ImageSource;

/**
 * @author 飘渺青衣
 * @see
 */
public interface IImageSourceService {

	/**
	 * 保存图片
	 * @author 飘渺青衣
	 * @version
	 * @param image
	 */
	public void save(ImageSource image);
	
	/**
	 * 更新素材ID
	 * @author 飘渺青衣
	 * @version
	 * @param id
	 * @param media_id
	 * @return 是否更新成功
	 */
	public boolean update(String id, String user_id, String media_id);
	
	/**
	 * 删除
	 * @author 飘渺青衣
	 * @version
	 * @param id
	 */
	public void remove(String id, String user_id);
	
	/**
	 * 删除
	 * @author 飘渺青衣
	 * @version
	 * @param ids
	 * @return
	 */
	public int remove(String[] ids, String user_id);
	
	/**
	 * 分页查询
	 * @author 飘渺青衣
	 * @version
	 * @param type
	 * @param page
	 * @param size
	 * @return
	 */
	public DataGrid<ImageSource> findPage(String type, String user_id, int page, int size);
	
	/**
	 * 上传图片
	 * @author 飘渺青衣
	 * @version
	 * @param type
	 * @param user_id
	 * @param dir
	 * @param file
	 * @return
	 */
	public int upload(String type, String user_id, String dir, String path, MultipartFile file);
}
