package com.dev.weixin.manager.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

/**
 * 图片源
 * @author 飘渺青衣
 * @see
 */
@Entity
@Table(name = "t_cdw_image")
public class ImageSource {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name="id", length = 100)
	private String id;
	
	@Column(name="name", length = 100, nullable = false)
	private String name;
	
	@Column(name="path", length = 150, nullable = false)
	private String path;
	
	@Column(name="user_id", length = 100, nullable = false)
	private String user_id;
	
	/**
	 * news：图文缩略图，materia：素材
	 */
	@Column(name="type", length = 20, nullable = false)
	private String type;
	
	@Column(name="contentType", length = 50, nullable = false)
	private String contentType;
	
	@Column(name="media_id", length = 100)
	private String media_id;
	
	@Column(name="createtime", length = 20, nullable = false)
	private String createtime;

	/**
	 * 是否发送到微信
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public boolean isUploadToWeixin() {
		return StringUtils.isNotEmpty(this.media_id);
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	public String getUser_id() {
		return user_id;
	}
	
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
