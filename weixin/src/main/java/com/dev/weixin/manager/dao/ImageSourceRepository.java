package com.dev.weixin.manager.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.dev.security.config.dao.BaseRepository;
import com.dev.weixin.manager.beans.ImageSource;

/**
 * @author 飘渺青衣
 * @see
 */
public interface ImageSourceRepository extends BaseRepository<ImageSource, String> {

	@Modifying
	@Query("delete from ImageSource where id in ?1 and user_id=?2")
	public int delete(String[] ids, String user_id);
	
	@Modifying
	@Query("delete from ImageSource where id=?1 and user_id=?2")
	public int delete(String id, String user_id);
	
	@Modifying
	@Query("update ImageSource set media_id=?3 where id=?1 and user_id=?2")
	public int update(String id, String user_id, String media_id);
	
	@Query("select ims from ImageSource ims where type=?1 and user_id=?2")
	public Page<ImageSource> findPage(String type, String user_id, Pageable page);
}
