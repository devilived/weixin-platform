package com.dev.weixin.manager.service;

import com.dev.security.util.DataGrid;
import com.dev.weixin.manager.beans.TextTemplate;

/**
 * @author 飘渺青衣
 * @see
 */
public interface ITextTemplateService {

	/**
	 * 保存文本模板
	 * @author 飘渺青衣
	 * @version
	 * @param text
	 */
	public void save(TextTemplate text);
	
	/**
	 * 获取文本模板
	 * @author 飘渺青衣
	 * @version
	 * @param id
	 * @param user_id
	 * @return
	 */
	public TextTemplate findTextTemplate(String id, String user_id);
	
	/**
	 * 分页信息
	 * @author 飘渺青衣
	 * @version
	 * @param cond 组合查询条件
	 * @param page
	 * @param size
	 * @return
	 */
	public DataGrid<TextTemplate> findPage(TextTemplate cond, int page, int size);
	
	/**
	 * 删除
	 * @author 飘渺青衣
	 * @version
	 * @param ids
	 * @param user_id
	 * @return 成功删除记录数
	 */
	public int remove(String[] ids, String user_id);
}
