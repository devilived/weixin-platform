package com.dev.weixin.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;

/**
 * @author 飘渺青衣
 * @see
 */
public class ObjectUtils {

	public static void trimToEmpty(Object obj) {
		Class<?> clazz = obj.getClass();
		Field[] fields = null;
		String value = null;
		while(clazz != Object.class) {
			fields = clazz.getDeclaredFields();
			if(fields != null) {
				for(Field field:fields) {
					if(field.getType() == String.class) {
						try {
							value = (String) clazz.getMethod(getter(field.getName())).invoke(obj);
							clazz.getMethod(setter(field.getName()), String.class)
								.invoke(obj, StringUtils.trimToEmpty(value));
						} catch (IllegalArgumentException e) {
						} catch (IllegalAccessException e) {
						} catch (NoSuchMethodException e) {
						} catch (SecurityException e) {
						} catch (InvocationTargetException e) {
						}
					}
				}
			}
			clazz = clazz.getSuperclass();
		}
	}
	
	private static String getter(String name) {
		StringBuilder s = new StringBuilder();
		s.append("get").append(name.substring(0, 1).toUpperCase());
		s.append(name.substring(1));
		return s.toString();
	}
	
	private static String setter(String name) {
		StringBuilder s = new StringBuilder();
		s.append("set").append(name.substring(0, 1).toUpperCase());
		s.append(name.substring(1));
		return s.toString();
	}
}
