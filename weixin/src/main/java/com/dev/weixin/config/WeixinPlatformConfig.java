package com.dev.weixin.config;

import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.dev.weixin.tools.models.WeixinConfig;

/**
 * 微信接口配置
 * @author 飘渺青衣
 * @see
 */
@Configuration
@PropertySource(
	value = {
		"classpath:/weixin.properties", 
		"classpath:/config.properties"
	}
)
@ComponentScan(basePackages = {
	"com.dev.weixin.**.handlers"
})
public class WeixinPlatformConfig {
	
	@Autowired
	Environment env;
	
	@Autowired
	private TransactionInterceptor transactionInterceptor;
	
	@Bean
	public WeixinConfig weixinConfig() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId(env.getProperty("weixin.appid"));
		config.setAppSecret(env.getProperty("weixin.appsecret"));
		config.setEncodingAESKey(env.getProperty("weixin.encodingAESKey"));
		config.setToken(env.getProperty("weixin.token"));
		return config;
	}
	
	@Bean
	public DefaultPointcutAdvisor handlerPointcutAdvisor() {
		DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor();
		advisor.setAdvice(transactionInterceptor);
		AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
		pointcut.setExpression("execution(* *..handler.*Service.*(..))");
		advisor.setPointcut(pointcut);
		advisor.setOrder(1);
		return advisor;
	}
	
	@Bean
	public MultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(env.getProperty("upload.max.size", Long.class));
		return resolver;
	}
}
