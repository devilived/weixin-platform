package com.dev.weixin.config.handlers;

import org.springframework.stereotype.Service;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.TextRes;
import com.dev.weixin.tools.service.GlobalReceiveHandler;

/**
 * @author 飘渺青衣
 * @see
 */
@Service
public class GlobalReceiveHandlerImpl implements GlobalReceiveHandler {

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.ReceiveHandler#handler(com.dev.weixin.tools.models.BaseMsg)
	 */
	@Override
	public BaseMsg handler(BaseMsg msg) {
		return new TextRes("感谢您发来的信息！");
	}

}
