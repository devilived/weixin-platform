package com.dev.weixin.platform.service.impl;

import java.io.UnsupportedEncodingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dev.weixin.platform.beans.WXAccessToken;
import com.dev.weixin.platform.beans.WXConfig;
import com.dev.weixin.platform.dao.WXAccessTokenRepository;
import com.dev.weixin.platform.dao.WXConfigRepository;
import com.dev.weixin.platform.service.IWXBaseService;
import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.base.AccessToken;
import com.dev.weixin.tools.service.UserService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;

/**
 * @author 飘渺青衣
 * @see
 */
@Service
public class WeixinBaseService extends BaseServiceImpl implements IWXBaseService {

	@Autowired
	private WXConfigRepository wxConfigRepository;
	
	@Autowired
	private WXAccessTokenRepository wxAccessTokenRepository;
	
	@Autowired
	private UserService userService;
	
	@Value("${server.prefix}")
	private String server_prefix;
	
	@Value("${server.domain}")
	private String server_domain;
	
	@Value("${server.contextpath}")
	private String server_contextpath;
	
	/**
	 * @param defaultConfig
	 */
	@Autowired
	public WeixinBaseService(WeixinConfig defaultConfig) {
		super(defaultConfig);
	}

	@Override
	protected void persistWeixinConfig(WeixinConfig config) {
		//持久化access_token
		AccessToken n_token = config.getAccessToken();
		WXAccessToken token = new WXAccessToken();
		token.setAppId(config.getAppId());
		token.setAccess_token(n_token.getAccess_token());
		token.setExpires_in(n_token.getExpires_in());
		token.setTime_in(n_token.getTime_in());
		this.wxAccessTokenRepository.save(token);
	}

	@Override
	protected WeixinConfig getPersistWeixinConfig(String toUserName) {
		WXConfig config = this.wxConfigRepository.findOne(StringUtils.trimToEmpty(toUserName));
		WeixinConfig wxconfig = new WeixinConfig();
		wxconfig.setAppId(config.getAppId());
		wxconfig.setAppSecret(config.getAppSecret());
		wxconfig.setEncodingAESKey(config.getEncodingAESKey());
		wxconfig.setToken(config.getToken());
		WXAccessToken token = this.wxAccessTokenRepository.findOne(config.getAppId());
		if(token != null) {
			AccessToken n_token = new AccessToken();
			n_token.setAccess_token(token.getAccess_token());
			n_token.setExpires_in(token.getExpires_in());
			n_token.setTime_in(token.getTime_in());
			//获取access_token
			wxconfig.setAccessToken(n_token);
		}
		return wxconfig;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.platform.service.IWXBaseService#getFullPath(java.lang.String)
	 */
	@Override
	public String getFullPath(String uri) {
		return new StringBuilder()
			.append(this.server_prefix)
			.append(this.server_domain)
			.append(this.server_contextpath)
			.append(uri).toString();
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.platform.service.IWXBaseService#buildOauth2Url(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String buildOauth2Url(String redirect_uri, String scope, String state) throws UnsupportedEncodingException {
		return this.userService.buildOauth2Url(this.getFullPath(redirect_uri), scope, state);
	}


}
