package com.dev.weixin.platform.dao;

import com.dev.security.config.dao.BaseRepository;
import com.dev.weixin.platform.beans.WXAccessToken;

/**
 * @author 飘渺青衣
 * @see
 */
public interface WXAccessTokenRepository extends BaseRepository<WXAccessToken, String> {

}
