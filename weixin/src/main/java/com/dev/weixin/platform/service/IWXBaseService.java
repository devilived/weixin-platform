package com.dev.weixin.platform.service;

import java.io.UnsupportedEncodingException;

/**
 * @author 飘渺青衣
 * @see
 */
public interface IWXBaseService {

	/**
	 * 获取完整路径
	 * @author 飘渺青衣
	 * @version
	 * @param uri
	 * @return
	 */
	public String getFullPath(String uri);

	/**
	 * 创建oauth2路径
	 * @author 飘渺青衣
	 * @version
	 * @param redirect_uri
	 * @param scope
	 * @param state
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String buildOauth2Url(String redirect_uri, String scope, String state) throws UnsupportedEncodingException;
}
