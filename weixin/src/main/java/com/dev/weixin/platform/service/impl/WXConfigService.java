package com.dev.weixin.platform.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.weixin.platform.beans.WXConfig;
import com.dev.weixin.platform.dao.WXConfigRepository;
import com.dev.weixin.platform.service.IWXConfigService;

/**
 * @author 飘渺青衣
 * @see
 */
@Service
public class WXConfigService implements IWXConfigService {

	@Autowired
	private WXConfigRepository wxConfigRepository;
	
	/* (non-Javadoc)
	 * @see com.dev.weixin.platform.service.IWXConfigService#findWXConfig(java.lang.String)
	 */
	@Override
	public WXConfig findWXConfig(String userId) {
		return this.wxConfigRepository.findOne(StringUtils.trimToEmpty(userId));
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.platform.service.IWXConfigService#findWXConfigByOriginalId(java.lang.String)
	 */
	@Override
	public WXConfig findWXConfigByOriginalId(String originalId) {
		return this.wxConfigRepository.findOneByOriginalId(StringUtils.trimToEmpty(originalId));
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.platform.service.IWXConfigService#save(com.dev.weixin.platform.beans.WXConfig)
	 */
	@Override
	public void save(WXConfig config) {
		this.wxConfigRepository.save(config);
	}

}
