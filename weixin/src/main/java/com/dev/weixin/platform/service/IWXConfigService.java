package com.dev.weixin.platform.service;

import com.dev.weixin.platform.beans.WXConfig;

/**
 * @author 飘渺青衣
 * @see
 */
public interface IWXConfigService {

	/**
	 * 获取微信配置
	 * @author 飘渺青衣
	 * @version
	 * @param userId
	 * @return
	 */
	public WXConfig findWXConfig(String userId);
	
	/**
	 * 获取微信配置
	 * @author 飘渺青衣
	 * @version
	 * @param originalId
	 * @return
	 */
	public WXConfig findWXConfigByOriginalId(String originalId);
	
	/**
	 * 保存
	 * @author 飘渺青衣
	 * @version
	 * @param config
	 */
	public void save(WXConfig config);
}
