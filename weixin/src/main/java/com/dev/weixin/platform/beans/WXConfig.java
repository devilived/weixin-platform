package com.dev.weixin.platform.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 我的公众号配置
 * @author 飘渺青衣
 * @see
 */
@Entity
@Table(name="t_cdw_wxconfig")
public class WXConfig {

	/**
	 * 用户ID，绑定
	 */
	@Id
	@Column(length = 50)
	private String userId;
	
	/**
	 * 原始ID
	 */
	@Column(length = 15, unique = true, nullable = false)
	private String originalId;
	
	/**
	 * 公众号名称
	 */
	@Column(length = 50, nullable = false)
	private String wxname;
	
	/**
	 * 微信号
	 */
	@Column(length = 100)
	private String wxno;
	
	/**
	 * 邮箱
	 */
	@Column(length = 255)
	private String email;
	
	/**
	 * 类型：数据字典DIC-WXTYPE
	 */
	@Column(length = 2, nullable = false)
	private String wxtype;
	
	/**
	 * 微信AppId
	 */
	@Column(length = 18, nullable = false)
	private String appId;
	
	/**
	 * 微信AppSecret
	 */
	@Column(length = 32, nullable = false)
	private String appSecret;
	
	/**
	 * 微信令牌
	 */
	@Column(length = 32, nullable = false)
	private String token;
	
	/**
	 * 微信EncodingAESKey
	 */
	@Column(length = 43, nullable = false)
	private String encodingAESKey;
	
	@Column(length = 20)
	private String createTime;

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getWxno() {
		return wxno;
	}

	public void setWxno(String wxno) {
		this.wxno = wxno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWxtype() {
		return wxtype;
	}

	public void setWxtype(String wxtype) {
		this.wxtype = wxtype;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEncodingAESKey() {
		return encodingAESKey;
	}

	public void setEncodingAESKey(String encodingAESKey) {
		this.encodingAESKey = encodingAESKey;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getWxname() {
		return wxname;
	}

	public void setWxname(String wxname) {
		this.wxname = wxname;
	}
	
}
