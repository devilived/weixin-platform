package com.dev.weixin.platform.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 飘渺青衣
 * @see
 */
@Entity
@Table(name = "t_cdw_wxat")
public class WXAccessToken {

	@Id
	@Column(length = 18)
	private String appId;
	
	/**
	 * 获取到的凭证
	 */
	@Column(length = 512)
	private String access_token;
	
	/**
	 * 凭证有效时间，单位：秒
	 */
	private long expires_in;
	
	/**
	 * 凭证获得时间
	 */
	private long time_in;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public long getTime_in() {
		return time_in;
	}

	public void setTime_in(long time_in) {
		this.time_in = time_in;
	}
	
}
