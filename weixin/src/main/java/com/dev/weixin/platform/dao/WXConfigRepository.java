package com.dev.weixin.platform.dao;

import com.dev.security.config.dao.BaseRepository;
import com.dev.weixin.platform.beans.WXConfig;

/**
 * @author 飘渺青衣
 * @see
 */
public interface WXConfigRepository extends BaseRepository<WXConfig, String> {

	public WXConfig findOneByOriginalId(String originalId);
	
}
