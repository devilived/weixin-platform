/**
 * 微信配置
 */
 ({
 	single:true,
 	launch:function() {
 		var me = this;
 		var perms = this.perms;
 		var wx = {
 			show:function() {
 				var footer = $('<div style="padding:5px;text-align:right;"></div>');
 				var win = me.createWindow('<div></div>', {
					title:"微信配置",
					width:300,
					height:400,
					resizable:false,
					minimizable:false,
					maximizable:false,
					footer:footer
				});
				
				var time = $("<span>&nbsp;</span>");
				var form = $('<form style="width:100%;" method="post"></form>').appendTo(win);
				form.form({
					url : me.ctx + "/wxconfig/save",
					success: function(data){
						$.messager.mask("close");
				        var data = eval('(' + data + ')');
				        if (data.success){
				        	time.text(data.message.createTime);
				            $.messager.alert('提示：', "微信配置保存成功！", "info");
				        } else {
				        	$.messager.alert('提示：', data.message == 1 ? "此微信配置已由他人填写！" : data.message, "info");
				        }
				    }
				});
				var table = $('<table width="100%" class="form-table"></table>').appendTo($('<div style="padding:5px 5px;"></div>').appendTo(form));
				var row = table.row();
				row.cell("原始ID：", {width:"80px"});
				var field = $('<input type="text" name="originalId" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[15]']
				});
				
				row = table.row();
				row.cell("公众号名称：");
				field = $('<input type="text" name="wxname" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[50]']
				});
				
				row = table.row();
				row.cell("微信号：");
				field = $('<input type="text" name="wxno" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[50]']
				});
				
				row = table.row();
				row.cell("Email：");
				field = $('<input type="text" name="email" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['email', 'maxlength[50]']
				});
				
				row = table.row();
				row.cell("公众号类型：");
				var source = '<select name="wxtype" style="width:79%;"></select>';
				var field = $(source);
				row.cell(field);
				field.combobox({
				    required:true,
					panelHeight:'auto',
				    url:me.ctx + '/dic/list',
				    queryParams:{
				    	parent:"DIC_WXTYPE"
				    },
				    valueField:'value',
				    textField:'label'
				});
				
				row = table.row();
				row.cell("AppId：");
				field = $('<input type="text" name="appId" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[18]']
				});
				
				row = table.row();
				row.cell("AppSecret：");
				field = $('<input type="text" name="appSecret" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[32]']
				});
				
				row = table.row();
				row.cell("Token：");
				field = $('<input type="text" name="token" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[32]']
				});
				
				row = table.row();
				row.cell("EncodingAESKey：");
				field = $('<input type="text" name="encodingAESKey" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[43]']
				});
				var addr = null;
				if(perms.view) {
					addr = $("<a href='#'>查看</a>"); 
					row = table.row();
					row.cell("接入地址：");
					row.cell(addr);
				}
				
				row = table.row();
				row.cell("创建时间：");
				row.cell(time);
				$.post(me.ctx + "/wxconfig/load", {}, function(json) {
					if(json.config) {
						form.form("load", json.config);
						time.text(json.config.createTime);
					}
					if(addr != null) {
						addr.click(function() {
							$.messager.alert('接入地址：',json.address);
							return false;
						});
					}
				}, "json");
				if(perms.save) {
					var saveButton = $('<a href="#">提交</a>').appendTo(footer);
					saveButton.linkbutton({
						iconCls : 'icon-ok',
						onClick : function() {
							if(form.form("validate")) {
								$.messager.mask().text("配置保存中，请稍后……");
								form.form("submit");
							}
							return false;						
						}
					});
					footer.append("&nbsp;");
				}
				var cancelButton = $('<a href="#">关闭</a>').appendTo(footer);
				cancelButton.linkbutton({
					iconCls: 'icon-cancel',
					onClick: function(){
						win.window("close");
						return false;						
					}
				});
 			}
 		};
 		wx.show();
 	}
 })