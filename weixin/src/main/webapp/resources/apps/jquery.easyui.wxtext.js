/**
 * 文本模板管理
 */
({
	launch : function() {
		var me = this;
		var perms = this.perms;
		var text = {
			browse : function() {
				var _this = this;
				var datagrid = $("<table></table>");
				var tools = $('<div></div>');
				if(perms.add) {
					var addButton = $('<a href="#" class="icon-add"></a>').appendTo(tools);
					addButton.click(function() {
						_this.add(datagrid);
						return false;
					});
				}
				if(perms.remove) {
					var rmButton = $('<a href="#" class="icon-remove"></a>').appendTo(tools);
					rmButton.click(function() {
						$.messager.confirm("请确认：", "确认要删除所选文本吗？", function(isOk) {
							if(isOk) {
								var items = datagrid.datagrid("getChecked");
								var ids = [];
								$(items).each(function() {
									ids.push(this.id);
								});
								$.messager.mask().text("操作执行中，请稍后……");
								$.post(me.ctx + "/weixin/text/remove", {ids : ids}, function(data) {
									if(data.success) {
										rmButton.hide();
										datagrid.datagrid("reload");
									}
									$.messager.mask('close');
								}, "json");							
							}
						});
						return false;
					});
				}
				var win = me.createWindow('<div></div>', {
					title:"文本消息",
					width:550,
					height:300,
					tools : tools
				});
				datagrid.appendTo(win);
				var columns = [
			    	{field : 'checkbox', checkbox : true},
			        {field : 'name', title : '文本名称', width:120},
			        {field : 'content', title : '文本内容', width:300}
				];
				if(perms.edit) {
					columns.push({
						field:'edit_button', title:"编辑", align:'center', formatter: function(value, row, index){
				        	return '<a class="l-btn l-btn-small l-btn-plain"><span class="l-btn-left l-btn-icon-left"><span class="l-btn-text l-btn-empty">&nbsp;</span><span class="l-btn-icon icon-edit">&nbsp;</span></span></a>';
					}});
				}
				datagrid.datagrid({
					fit : true,
					url : me.ctx + "/weixin/text/browse",
					pagination : true,
					pageSize : 10,
					selectOnCheck : false,
					checkOnSelect : false,
					singleSelect : true,
				    columns : [columns],
				    onClickCell : function(index, field, value) {
				    	var rows = datagrid.datagrid("getRows");
				    	if(field === "edit_button") {
				    		_this.edit(datagrid, rows[index]);
				    	}
				    },
				    onCheckAll : function(rows) {
				    	if(rows.length > 0 && perms.remove) {
				    		rmButton.show();
				    	}
				    },
				    onUncheckAll : function(rows) {
				    	if(perms.remove) {
				    		rmButton.hide();
				    	}
				    },
				    onCheck : function(index, row) {
				    	if(perms.remove) {
				    		rmButton.show();
				    	}
				    },
				    onUncheck : function(index, row) {
				    	if(perms.remove && $(this).datagrid("getChecked").length < 1) {
				    		rmButton.hide();
				    	}
				    }, 
				    onLoadSuccess : function() {
				    	if(perms.remove) {
				    		rmButton.hide();
				    	}
				    }
				});
				return win;
			},
			add:function(datagrid) {
				var _this = this;
				var footer = $('<div style="padding:5px;text-align:right;"></div>');
				var win = me.createWindow('<div></div>', {
					title:"文本添加",
					width:300,
					height:180,
					resizable:false,
					minimizable:false,
					maximizable:false,
					modal:true,
					footer:footer
				});
				var form = $('<form style="width:100%;" method="post"></form>').appendTo(win);
				form.form({
					url : me.ctx + "/weixin/text/create",
					success: function(data){
						$.messager.mask("close");
				        var data = eval('(' + data + ')');
				        if (data.success){
				        	datagrid.datagrid("options").pageNumber = 1;
				            datagrid.datagrid("reload");
				            win.window("close");
				        } else {
				        	$.messager.alert('提示：', data.message, "info");
				        }
				    }
				});
				var table = $('<table width="100%" class="form-table"></table>').appendTo($('<div style="padding:5px 5px;"></div>').appendTo(form));
				var row = table.row();
				row.cell("文本名称：", {width:"60px"});
				var field = $('<input type="text" name="name" />');
				row.cell(field);
				field.textbox({
					required: true,
					validType : ['maxlength[20]']
				});
				row = table.row();
				row.cell("文本内容：");
				field = $('<input type="text" name="content" style="height:80px" />');
				row.cell(field);
				field.textbox({
					multiline:true,
					required: true,
					validType : ['maxlength[2000]']
				});
				var saveButton = $('<a href="#">提交</a>').appendTo(footer);
				saveButton.linkbutton({
					iconCls : 'icon-ok',
					onClick : function() {
						if(form.form("validate")) {
							$.messager.mask().text("操作执行中，请稍后……");
							form.form("submit");
						}
						return false;						
					}
				});
				footer.append("&nbsp;");
				var cancelButton = $('<a href="#">取消</a>').appendTo(footer);
				cancelButton.linkbutton({
					iconCls: 'icon-cancel',
					onClick: function(){
						win.window("close");
						return false;						
					}
				});
			},
			edit:function(datagrid, text) {
				var _this = this;
				var footer = $('<div style="padding:5px;text-align:right;"></div>');
				var win = me.createWindow('<div></div>', {
					title:"文本编辑",
					width:300,
					height:180,
					resizable:false,
					minimizable:false,
					maximizable:false,
					modal:true,
					footer:footer
				});
				var form = $('<form style="width:100%;" method="post"></form>').appendTo(win);
				form.form({
					url : me.ctx + "/weixin/text/update",
					success: function(data){
						$.messager.mask("close");
				        var data = eval('(' + data + ')');
				        if (data.success){
				        	datagrid.datagrid("options").pageNumber = 1;
				            datagrid.datagrid("reload");
				            win.window("close");
				        } else {
				        	$.messager.alert('提示：', data.message, "info");
				        }
				    }
				});
				var table = $('<table width="100%" class="form-table"></table>').appendTo($('<div style="padding:5px 5px;"></div>').appendTo(form));
				var row = table.row();
				row.cell("文本名称：", {width:"60px"});
				var field = $('<input type="text" name="name" />');
				var fieldContainer = row.cell(field);
				$('<input type="hidden" name="id" />').appendTo(fieldContainer);
				field.textbox({
					required: true,
					validType : ['maxlength[20]']
				});
				row = table.row();
				row.cell("文本内容：");
				field = $('<input type="text" name="content" style="height:80px" />');
				row.cell(field);
				field.textbox({
					multiline:true,
					required: true,
					validType : ['maxlength[2000]']
				});
				var saveButton = $('<a href="#">提交</a>').appendTo(footer);
				saveButton.linkbutton({
					iconCls : 'icon-ok',
					onClick : function() {
						if(form.form("validate")) {
							$.messager.mask().text("操作执行中，请稍后……");
							form.form("submit");
						}
						return false;						
					}
				});
				footer.append("&nbsp;");
				var cancelButton = $('<a href="#">取消</a>').appendTo(footer);
				cancelButton.linkbutton({
					iconCls: 'icon-cancel',
					onClick: function(){
						win.window("close");
						return false;						
					}
				});
				form.form("load", text);
			}
		};
		text.browse();
	}
})