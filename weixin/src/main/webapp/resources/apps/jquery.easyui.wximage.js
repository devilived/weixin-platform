/**
 * 微信配置
 */
 ({
 	single:true,
 	launch:function() {
 		var me = this;
 		var perms = this.perms;
 		var isFirst = true;
 		var defaultOptions = {
 			type:"none",
 			callback:function(image) {}
 		};
 		$.extend(defaultOptions, arguments[0]);
 		var image = {
 			show:function() {
 				var _this = this;
 				var win = me.createWindow('<div style="overflow-x:hidden;"></div>', {
					title:"图片管理",
					width:490,
					height:400,
					resizable:false,
					minimizable:false,
					modal:true
				});
				var tabs = $('<div><div title="图片列表"></div><div title="上传图片"></div></div>').appendTo(win);
				var items = tabs.children("div");
				var list = items.eq(0);
				var upload = items.eq(1);
				var imageList = null;
				var isUploaderFirst = true;
				tabs.tabs({
					fit:true,
				    border:false,
				    onSelect:function(title){
				        if(title == "图片列表") {
				        	if(isFirst) {
				        		imageList = new ImageList(list);
				        	} else {
				        		imageList.reload(1);
				        	}
				        } else if(title == "上传图片" && isUploaderFirst) {
				        	isUploaderFirst = false;
				        	new Uploader(upload);
				        }
				        isFirst = false;
				    }
				});
 			}
 		};
 		var ImageList = function(container) {
 			var _this = this;
 			var layout = $('<div data-options="region:\'center\'" style="position:relative;"></div><div data-options="region:\'south\'" style="height:32px;"></div>').appendTo(container);
 			var layouts = container.children("div"); 
 			var center = layouts.eq(0);
 			var south = layouts.eq(1);
 			container.layout({
 				fit:true
 			});
 			var list = $('<ul class="image-list"></ul>').appendTo(center);
 			var pager = $('<div style="background:#efefef;"></div>').appendTo(south);
 			pager.pagination({
 				total:1,
 				pageSize:9,
 				onSelectPage:function(pageNumber, pageSize) {
 					_this.reload(pageNumber);
 				}
 			});
 			
 			this.reload = function(page) {
 				pager.pagination("loading");
 				var options = pager.pagination("options");
 				$.post(me.ctx + "/weixin/image/browse", {
 					type:defaultOptions.type,
 					page:page,
 					rows:options.pageSize
 				}, function(data) {
 					pager.pagination("refresh", {
 						pagerNumber:page,
 						total:data.total
 					});
 					list.children("li").remove();
 					$(data.rows).each(function() {
 						var json = this;
 						var row = $('<li><div><img src="' + me.ctx + this.path + '" /></div><div><a href="#">删除</a></div></li>').appendTo(list);
 						row.find("img").click(function() {
 							defaultOptions.callback(json);
 						});
 						row.find("a").click(function() {
 							$.messager.confirm("请确认：", "确认要删除图片吗？", function(isOk) {
 								if(isOk) {
 									$.messager.mask().text("操作执行中，请稍后……");
		 							$.post(me.ctx + "/weixin/image/remove", {id:json.id}, function(data) {
		 								_this.reload(page);
		 								$.messager.mask('close');
		 							}, "json");
 								}
 							});
 							return false;
 						});
 					});
 					pager.pagination("loaded");
 					
 				}, "json");
 			}
 			this.reload(1);
 		}
 		
 		var Uploader = function(container) {
 			var _this = this;
 			var idpre = uuid.create() + "_";
 			var layout = $('<div data-options="region:\'north\'" style="height:115px;overflow-y:hidden;"></div><div data-options="region:\'center\'" style="position:relative;padding:5px 5px;"></div>').appendTo(container);
 			var layouts = container.children("div");
 			var north = layouts.eq(0);
 			var center = layouts.eq(1);
 			container.layout({
 				fit:true
 			});
 			var browser = $('<div class="uploader"><div>拖拽图片到这里</div><div class="or">-或者-</div><div class="browser"><label><span>选择图片</span><input type="file" name="file" multiple="multiple" title="点击选择图片"></label></div></div>').appendTo(north);
 			browser.dmUploader({
		        url: me.ctx + "/weixin/image/upload",
		        extraData: {
		        	type:defaultOptions.type
		        },
		        dataType: 'json',
		        allowedTypes: 'image/*',
		        /*extFilter: 'jpg;png;gif',*/
		        onInit: function(){
		        	
		        },
		        onBeforeUpload: function(id){
		        	
		        },
		        onNewFile: function(id, file){
		        	$('<div class="uploading"><img class="image" id="image'+ idpre + id + '" /><span class="name">'+file.name+'</span><br/>状态：<span id="status'+ idpre + id + '"></span><div id="process'+ idpre + id + '"></div></div>').prependTo(center);
		        	if (typeof FileReader !== "undefined"){
			            var reader = new FileReader();
			            // Last image added
			            var img = $('#image' + idpre + id);
			            reader.onload = function (e) {
			              img.attr('src', e.target.result);
			            }
			            reader.readAsDataURL(file);
			        }
			        $('#status' + idpre + id).text("正在上传");
			        $('#process' + idpre + id).progressbar({
			        	value:0
			        });
		        },
		        onComplete: function(){
		        	
		        },
		        onUploadProgress: function(id, percent){
		        	$('#process' + idpre + id).progressbar("setValue", percent);
		        },
		        onUploadSuccess: function(id, data){
					$('#process' + idpre + id).progressbar("setValue", 100);
					$('#status' + idpre + id).text("上传完毕");
		        },
		        onUploadError: function(id, message){
		        	$.messager.alert('提示：', message, "error");
		        },
		        onFileTypeError: function(file){
		        	$.messager.alert('提示：', "请选择图片上传！", "warning");
		        },
		        onFileSizeError: function(file){
		        	$.messager.alert('提示：', "您选择的图片过大！", "warning");
		        },
		        onFallbackMode: function(message){
		        	$.messager.alert('提示：', message, "warning");
		        }
		    });
 		}
 		image.show();
 	}
 })