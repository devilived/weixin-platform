<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><spring:message code="project.title" /></title>
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/gooey/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/gooey/css/gooey.min.css">
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/gooey/css/gooey.menu.css">
		
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/jquery-easyui/themes/bootstrap/easyui.css">
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/jquery-easyui/themes/icon.css">
		
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/css/common.css">
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/css/loading.css">
		
		<link rel="stylesheet" type="text/css" href="<%=path %>/resources/css/weixin.css">
		
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.min.js"></script>
		
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.validator.js"></script>
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.window.js"></script>
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/locale/easyui-lang-zh_CN.js"></script>
		
		<script type="text/javascript" src="<%=path %>/resources/gooey/gooey.min.js"></script>
		
		<script type="text/javascript" src="<%=path %>/resources/uploader/dmuploader.min.js"></script>
		
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.mask.js"></script>
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.taskbar.js"></script>
		<script type="text/javascript" src="<%=path %>/resources/jquery-easyui/jquery.easyui.desktop.js"></script>
	</head>
	<body>
	
	</body>
</html>