package com.dev.weixin.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.dev.security.config.RootConfig;
import com.dev.weixin.tools.service.ReceiveHandler;
import com.dev.weixin.tools.service.ReceiveService;

@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TestSpring extends AbstractTestNGSpringContextTests {

	@Autowired
	private ReceiveService receiveService;
	
	@Autowired
	private ReceiveHandler<?>[] receiveHandlers;
	
	@Test
	public void test() {
		Assert.assertNotNull(receiveService);
		for(int i=0; i<this.receiveHandlers.length; i++) {
			System.out.println(this.receiveHandlers[i].getClass());
		}
	}
	
}
