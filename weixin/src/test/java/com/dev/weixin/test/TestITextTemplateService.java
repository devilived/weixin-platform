package com.dev.weixin.test;

import java.util.Calendar;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.dev.security.config.RootConfig;
import com.dev.security.util.DataGrid;
import com.dev.weixin.manager.beans.TextTemplate;
import com.dev.weixin.manager.service.ITextTemplateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 飘渺青衣
 * @see
 */
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TestITextTemplateService extends AbstractTestNGSpringContextTests {

	@Autowired
	private ITextTemplateService textTemplateService;
	
	@Autowired
	private ObjectMapper om;
	
	@Test
	public void testSave() {
		TextTemplate text = new TextTemplate();
		text.setName("模板名称1");
		text.setContent("内容1");
		text.setUser_id("1b8f80034d8a0193014d8a1207490001");
		text.setCreateTime(DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
		this.textTemplateService.save(text);
		Assert.assertNotNull(text.getId());
	}
	
	@Test
	public void testFindTextTemplate() {
		TextTemplate text = this.textTemplateService.findTextTemplate("4028cb814e4ef8cf014e4ef8e2660000", "1b8f80034d8a0193014d8a1207490001");
		Assert.assertNotNull(text);
	}
	
	@Test
	public void testFindPage() throws JsonProcessingException {
		TextTemplate cond = new TextTemplate();
		cond.setUser_id("1b8f80034d8a0193014d8a1207490001");
		cond.setName("名");
		cond.setContent("容");
		DataGrid<TextTemplate> datagrid = this.textTemplateService.findPage(cond, 1, 10);
		Assert.assertNotNull(datagrid);
		System.out.println(om.writeValueAsString(datagrid));
	}
	
	@Test
	public void testRemove() {
		Assert.assertTrue(this.textTemplateService.remove(new String[]{"4028cb814e4ef8cf014e4ef8e2660000"}, "1b8f80034d8a0193014d8a1207490001") > 0);
	}
}
