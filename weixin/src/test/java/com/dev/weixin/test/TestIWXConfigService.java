package com.dev.weixin.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.dev.security.config.RootConfig;
import com.dev.weixin.platform.beans.WXConfig;
import com.dev.weixin.platform.service.IWXConfigService;

/**
 * @author 飘渺青衣
 * @see
 */
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TestIWXConfigService extends AbstractTestNGSpringContextTests {

	@Autowired
	private IWXConfigService wxConfigService;
	
	@Test
	public void testFindWXConfig() {
		WXConfig config = this.wxConfigService.findWXConfig("111");
		Assert.assertNull(config);
	}
}
