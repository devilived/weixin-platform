package com.dev.weixin.test;

import java.util.Calendar;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.dev.security.config.RootConfig;
import com.dev.security.util.DataGrid;
import com.dev.weixin.manager.beans.ImageSource;
import com.dev.weixin.manager.service.IImageSourceService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 飘渺青衣
 * @see
 */
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TestIImageSourceService extends AbstractTestNGSpringContextTests {

	@Autowired
	private IImageSourceService imageSourceService;
	
	@Autowired
	private ObjectMapper om;
	
	@Test
	public void testSave() {
		ImageSource image = new ImageSource();
		image.setName("google.jpg");
		image.setPath("/resources/images/upload/kdkdkdk/jjijiage.jpg");
		image.setType("news");
		image.setUser_id("1b8f80034d8a0193014d8a1207490001");
		image.setCreatetime(DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
		this.imageSourceService.save(image);
	}
	
	@Test
	public void testUpdate() {
		this.imageSourceService.update("4028cb814e4eec73014e4eec89600000", "1b8f80034d8a0193014d8a1207490001", "dkdkdkkdkdkd");
	}
	
	@Test
	public void testFindPage() throws JsonProcessingException {
		DataGrid<ImageSource> dg = this.imageSourceService.findPage("news", "1b8f80034d8a0193014d8a1207490001", 1, 10);
		Assert.assertNotNull(dg);
		System.out.println(om.writeValueAsString(dg));
	}
	
	@Test
	public void testRemove() {
		this.imageSourceService.remove("4028cb814e4eec73014e4eec89600000", "1b8f80034d8a0193014d8a1207490001");
	}
	
	@Test
	public void testRemoves() {
		this.imageSourceService.remove(new String[]{
				"4028cb814e4eed12014e4eed24db0000",
				"4028cb814e4eed42014e4eed55e30000"
		}, "1b8f80034d8a0193014d8a1207490001");
	}
	
}
