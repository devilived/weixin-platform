package com.dev.weixin.test;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import com.dev.security.config.RootConfig;
import com.dev.weixin.platform.service.IWXBaseService;

/**
 * @author 飘渺青衣
 * @see
 */
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TestIWXBaseService extends AbstractTestNGSpringContextTests {

	@Autowired
	private IWXBaseService wxBaseService;
	
	@Test
	public void testGetFullPath() {
		System.out.println(this.wxBaseService.getFullPath("/user/create?aaaa=388383"));
	}
	
	@Test
	public void testBuildOauth2Url() throws UnsupportedEncodingException {
		System.out.println(this.wxBaseService.buildOauth2Url("/user/create?aaa=3333", "ssss", ""));
	}
}
