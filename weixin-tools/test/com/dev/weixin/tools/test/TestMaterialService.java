package com.dev.weixin.tools.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.material.GraphicsTextMaterial;
import com.dev.weixin.tools.models.material.GraphicsTextMaterials;
import com.dev.weixin.tools.models.material.Material;
import com.dev.weixin.tools.models.material.MaterialCount;
import com.dev.weixin.tools.models.material.VideoMaterial;
import com.dev.weixin.tools.models.material.VideoMaterialGet;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.MaterialService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.MaterialServiceImpl;

/**
 * @author 飘渺青衣
 * @see
 */
public class TestMaterialService {

	private BaseService service;
	
	private MaterialService materialService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		materialService = new MaterialServiceImpl(service);
	}
	
	@Test
	public void testAddTempMaterial() throws ErrorMsgException {
		Material temp = this.materialService.addTempMaterial("image", new File("d:\\temp\\temp.jpg"));
		Assert.assertNotNull(temp);
	}
	
	@Test
	public void testGetTempMaterial() throws ErrorMsgException {
		File file = this.materialService.getTempMaterial("liE1D8e_nSz7f33sMh4WazVgr4d_1j1MHZqD62A5iedwdeDDx8naGtyHv0bLoNOm", "D:\\temp\\");
		Assert.assertNotNull(file);
	}
	
	@Test
	public void testAddMaterial() throws ErrorMsgException {
		Material material = this.materialService.addMaterial("image", new File("d:\\temp\\temp.jpg"));
		Assert.assertNotNull(material);
		//lLnNTlgdGp3zUS3ZFC_zVhDAWM7IsEg_jjOBGw2eAo0
	}
	
	@Test
	public void testAddNews() throws ErrorMsgException {
		List<GraphicsTextMaterial> materials = new ArrayList<GraphicsTextMaterial>();
		GraphicsTextMaterial m = new GraphicsTextMaterial();
		m.setAuthor("DEVILIVED");
		m.setContent("内容1");
		m.setTitle("测试标题");
		m.setThumb_media_id("Ear-Am3_p81L9E-t5HX8BqNbKgJSnXG2VscvMuOMBEs");
		materials.add(m);
		m = new GraphicsTextMaterial();
		m.setThumb_media_id("Ear-Am3_p81L9E-t5HX8BqNbKgJSnXG2VscvMuOMBEs");
		m.setAuthor("DEVILIVED");
		m.setContent("内容1");
		m.setTitle("测试标题2");
		materials.add(m);
		GraphicsTextMaterials result = this.materialService.addNews(materials);
		Assert.assertNotNull(result);
		Assert.assertNotNull(result.getMedia_id());
		//Ear-Am3_p81L9E-t5HX8BqY-CCiDwV3Q9j1L6vdyraQ
	}
	
	@Test
	public void testAddVideoMaterial() throws ErrorMsgException {
		VideoMaterial video = this.materialService.addVideoMaterial("视频标题", "视频简介", new File("D:\\temp\\test.avi"));
		Assert.assertNotNull(video);
		Assert.assertNotNull(video.getMedia_id());
		//lLnNTlgdGp3zUS3ZFC_zVpShwUuxpkjR8DyMDTfHhyw
	}
	
	@Test
	public void testGetGraphicsTextMaterials() throws ErrorMsgException {
		GraphicsTextMaterials m = this.materialService.getGraphicsTextMaterials("Ear-Am3_p81L9E-t5HX8BqY-CCiDwV3Q9j1L6vdyraQ");
		Assert.assertNotNull(m);
		Assert.assertNotNull(m.getMedia_id());
	}
	
	@Test
	public void testGetVideoMaterial() throws ErrorMsgException {
		VideoMaterialGet video = this.materialService.getVideoMaterial("lLnNTlgdGp3zUS3ZFC_zVpShwUuxpkjR8DyMDTfHhyw");
		Assert.assertNotNull(video);
		Assert.assertNotNull(video.getMedia_id());
	}
	
	@Test
	public void testGetMaterial() throws ErrorMsgException {
		File file = this.materialService.getMaterial("lLnNTlgdGp3zUS3ZFC_zVldvMBp6Pkas7OlBznC30_s", "d:\\temp\\material.jpg");
		Assert.assertNotNull(file);
		Assert.assertTrue(file.exists());
	}
	
	@Test
	public void testRemoveMaterial() throws ErrorMsgException {
		Assert.assertTrue(this.materialService.removeMaterial("lLnNTlgdGp3zUS3ZFC_zVldvMBp6Pkas7OlBznC30_s"));
	}
	
	@Test
	public void testGetMaterialCount() throws ErrorMsgException {
		MaterialCount count = this.materialService.getMaterialCount();
		Assert.assertNotNull(count);
	}
}
