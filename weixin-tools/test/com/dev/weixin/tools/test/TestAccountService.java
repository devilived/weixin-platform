package com.dev.weixin.tools.test;

import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.account.Ticket;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.service.AccountService;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.impl.AccountServiceImpl;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;

/**
 * 
 * @author 飘渺青衣
 * @see
 */
public class TestAccountService {
	
	private BaseService service;
	
	private AccountService accountService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		accountService = new AccountServiceImpl(service);
	}
	
	@Test
	public void testCreateTempTicket() throws ErrorMsgException, UnsupportedEncodingException {
		Ticket ticket = this.accountService.createTempTicket(1800, 1000);
		Assert.assertNotNull(ticket);
		System.out.println(this.accountService.getUrl(ticket.getTicket()));
	}
	
	@Test
	public void testCreateTicket() throws ErrorMsgException, UnsupportedEncodingException {
		Ticket ticket = this.accountService.createTicket("1000000");
		Assert.assertNotNull(ticket);
		System.out.println(this.accountService.getUrl(ticket.getTicket()));
	}
	
	@Test
	public void testGetShortUrl() throws ErrorMsgException {
		String short_url = this.accountService.getShortUrl("http://game.ali213.net/forum-934-1.html");
		Assert.assertNotNull(short_url);
	}
	
}
