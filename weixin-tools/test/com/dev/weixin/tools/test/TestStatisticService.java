/**
 * Copyright (c)2002-2006 by OpenSymphony
 * All rights reserved.
 */
package com.dev.weixin.tools.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.statistic.ArticleSummary;
import com.dev.weixin.tools.models.statistic.ArticleTotal;
import com.dev.weixin.tools.models.statistic.Decrease;
import com.dev.weixin.tools.models.statistic.GrandTotal;
import com.dev.weixin.tools.models.statistic.InterfaceSummary;
import com.dev.weixin.tools.models.statistic.UpStreamMsg;
import com.dev.weixin.tools.models.statistic.UpStreamMsgDist;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.StatisticService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.StatisticServiceImpl;

/**
 * @author 飘渺青衣
 * @see
 */
public class TestStatisticService {
	
	private BaseService service;
	
	private StatisticService statisticService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		statisticService = new StatisticServiceImpl(service);
	}
	
	@Test
	public void testGetUserSummary() throws ErrorMsgException {
		List<Decrease> list = this.statisticService.getUserSummary("2015-04-04", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUserCumulate() throws ErrorMsgException {
		List<GrandTotal> list = this.statisticService.getUserCumulate("2015-04-04", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetArticleSummary() throws ErrorMsgException {
		List<ArticleSummary> list = this.statisticService.getArticleSummary("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetArticleTotal() throws ErrorMsgException {
		List<ArticleTotal> list = this.statisticService.getArticleTotal("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUserRead() throws ErrorMsgException {
		List<ArticleSummary> list = this.statisticService.getUserRead("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUserReadHour() throws ErrorMsgException {
		List<ArticleSummary> list = this.statisticService.getUserReadHour("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUserShare() throws ErrorMsgException {
		List<ArticleSummary> list = this.statisticService.getUserShare("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUserShareHour() throws ErrorMsgException {
		List<ArticleSummary> list = this.statisticService.getUserShareHour("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsg() throws ErrorMsgException {
		List<UpStreamMsg> list = this.statisticService.getUpStreamMsg("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgHour() throws ErrorMsgException {
		List<UpStreamMsg> list = this.statisticService.getUpStreamMsgHour("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgWeek() throws ErrorMsgException {
		List<UpStreamMsg> list = this.statisticService.getUpStreamMsgWeek("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgMonth() throws ErrorMsgException {
		List<UpStreamMsg> list = this.statisticService.getUpStreamMsgMonth("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgDist() throws ErrorMsgException {
		List<UpStreamMsgDist> list = this.statisticService.getUpStreamMsgDist("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgDistWeek() throws ErrorMsgException {
		List<UpStreamMsgDist> list = this.statisticService.getUpStreamMsgDistWeek("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetUpStreamMsgDistMonth() throws ErrorMsgException {
		List<UpStreamMsgDist> list = this.statisticService.getUpStreamMsgDistMonth("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetInterfaceSummary() throws ErrorMsgException {
		List<InterfaceSummary> list = this.statisticService.getInterfaceSummary("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
	
	@Test
	public void testGetInterfaceSummaryHour() throws ErrorMsgException {
		List<InterfaceSummary> list = this.statisticService.getInterfaceSummaryHour("2015-04-05", "2015-04-05");
		Assert.assertNotNull(list);
	}
}
