package com.dev.weixin.tools.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.menu.Button;
import com.dev.weixin.tools.models.menu.Menu;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.MenuService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.MenuServiceImpl;

/**
 * 
 * @author 飘渺青衣
 * @see
 */
public class TestMenuService {
	
	private BaseService service;
	
	private MenuService menuService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		menuService = new MenuServiceImpl(service);
	}
	
	@Test
	public void testCreate() throws ErrorMsgException {
		Menu menu = new Menu();
		menu.getButton().add(new Button("目录1", "BUTTON1"));
		menu.getButton().add(new Button("目录2", "BUTTON2"));
		menu.getButton().add(new Button("目录3", "BUTTON3"));
		this.menuService.create(menu);
	}
	
	@Test
	public void testGet() throws ErrorMsgException {
		Menu menu = this.menuService.get();
		Assert.assertNotNull(menu);
		Assert.assertNotNull(menu.getButton());
	}
	
	@Test
	public void testRemove() throws ErrorMsgException {
		Assert.assertTrue(this.menuService.remove());
	}
}
