package com.dev.weixin.tools.test;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.user.Group;
import com.dev.weixin.tools.models.user.UserInfo;
import com.dev.weixin.tools.models.user.UserList;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.UserService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.UserServiceImpl;

/**
 * 
 * @author 飘渺青衣
 * @see
 */
public class TestUserService {
	
	private BaseService service;
	
	private UserService userService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		userService = new UserServiceImpl(service);
	}
	
	@Test
	public void testCreateGroup() throws ErrorMsgException {
		Group group = new Group();
		group.setName("用户分组1");
		group = this.userService.createGroup(group);
		Assert.assertNotNull(group);
	}
	
	@Test
	public void testGetGroups() throws ErrorMsgException {
		List<Group> groups = this.userService.getGroups();
		Assert.assertNotNull(groups);
		Assert.assertTrue(groups.size() > 0);
	}
	
	@Test
	public void testGetGroupId() throws ErrorMsgException {
		String groupid = this.userService.getGroupId("oi3bLt-vePuZHQI8Fpvs077_X0oM");
		Assert.assertNotNull(groupid);
	}
	
	@Test
	public void testUpdateGroup() throws ErrorMsgException {
		Group group = new Group();
		group.setId("100");
		group.setName("测试分组");
		Assert.assertTrue(this.userService.updateGroup(group));
	}
	
	@Test
	public void testMoveUserToGroup() throws ErrorMsgException {
		Assert.assertTrue(this.userService.moveUserToGroup("oi3bLt-vePuZHQI8Fpvs077_X0oM", "100"));
		testGetGroupId();
		List<String> openids = new ArrayList<String>();
		openids.add("oi3bLt-vePuZHQI8Fpvs077_X0oM");
		Assert.assertTrue(this.userService.moveUserToGroup(openids, "2"));
		testGetGroupId();
	}
	
	@Test
	public void testUpdateUserRemark() throws ErrorMsgException {
		Assert.assertTrue(this.userService.updateUserRemark("oi3bLt-vePuZHQI8Fpvs077_X0oM", "�µı�ע"));
	}
	
	@Test
	public void testGetUserInfo() throws ErrorMsgException {
		UserInfo user = this.userService.getUserInfo("oi3bLt-vePuZHQI8Fpvs077_X0oM", null);
		Assert.assertNotNull(user);
	}
	
	@Test
	public void testGetUserList() throws ErrorMsgException {
		UserList list = this.userService.getUserList("");
		Assert.assertNotNull(list);
//		Assert.assertTrue(!list.hasNext());
	}
	
	@Test
	public void testBuildOauth2Url() throws UnsupportedEncodingException {
		String uri = "http://www.ichuncheng.com.cn/wxpt/jsjr/mine.do?minePage";
		System.out.println(this.userService.buildOauth2Url(uri, "snsapi_base", ""));
	}
	
}
