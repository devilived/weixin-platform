/**
 * Copyright (c)2002-2006 by OpenSymphony
 * All rights reserved.
 */
package com.dev.weixin.tools.test;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.receive.event.PicSysphotoEvent;
import com.dev.weixin.tools.service.event.PicWeixinHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author ��Ӣ
 * @see
 */
public class PicWeixinHandlerImpl implements PicWeixinHandler {

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.ReceiveHandler#handler(com.dev.weixin.tools.models.BaseMsg)
	 */
	@Override
	public BaseMsg handler(PicSysphotoEvent msg) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(msg));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
