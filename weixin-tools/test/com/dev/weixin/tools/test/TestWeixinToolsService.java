package com.dev.weixin.tools.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.base.IpAddress;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.receive.ValidData;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.ReceiveService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.ReceiveServiceImpl;
import com.dev.weixin.tools.utils.sha1.AesException;

/**
 * 
 * @author 飘渺青衣
 * @see
 */
public class TestWeixinToolsService {
	
	private BaseService service;
	
	private ReceiveService receiveService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		receiveService = new ReceiveServiceImpl(service);
		ReceiveServiceImpl rsi = (ReceiveServiceImpl) this.receiveService;
		rsi.setReceiveCheckSignature(false);
		rsi.setTextHandler(new TextHandlerImpl());
		rsi.setLocationSelectHandler(new LocationSelectHandlerImpl());
		rsi.setPicWeixinHandler(new PicWeixinHandlerImpl());
	}

	@Test
	public void testBuildSignature() throws AesException {
		ValidData valid = new ValidData();
		valid.setNonce("111111");
		valid.setTimestamp(String.valueOf(System.currentTimeMillis()));
		valid.setSignature(this.receiveService.buildSignature(valid));
		valid.setEchostr("123456789");
		System.out.println("http://weixin.digitaljilin.com/jlsi/weixin?signature=" + valid.getSignature() + "&timestamp=" + valid.getTimestamp() + "&nonce="+valid.getNonce() + "&echostr=" + valid.getEchostr());
	}
	
	@Test
	public void testGetAccessToken() throws ErrorMsgException {
		String access_token = service.getAccessToken();
		Assert.assertNotNull(access_token);
		System.out.println(access_token);
	}
	
	@Test
	public void testGetIpAddress() throws ErrorMsgException {
		IpAddress ips = service.getIpAddress();
		Assert.assertNotNull(ips);
		Assert.assertNotNull(ips.getIp_list());
		System.out.println(ips.getIp_list());
	}
	
	@Test
	public void testReceive() throws AesException, IOException {
		FileInputStream fis;
		try {
			fis = new FileInputStream("d:/temp/pic_weixin.xml");
			System.out.println(this.receiveService.receive(null, fis));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ErrorMsgException e) {
			e.printStackTrace();
		}
	}
}
