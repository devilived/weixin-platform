package com.dev.weixin.tools.test;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.receive.common.TextMsg;
import com.dev.weixin.tools.service.common.TextHandler;

/**
 * @author 飘渺青衣
 * @see
 */
public class TextHandlerImpl implements TextHandler {

	@Override
	public BaseMsg handler(TextMsg msg) {
		System.out.println(msg.getContent());
		return null;
	}

}
