package com.dev.weixin.tools.test;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Assert;
import org.junit.Test;

import com.dev.weixin.tools.models.customer.KFNewsMsg;
import com.dev.weixin.tools.models.customer.submodel.News;
import com.dev.weixin.tools.models.receive.EventMsg;
import com.dev.weixin.tools.models.response.NewsRes;
import com.dev.weixin.tools.models.response.VideoRes;
import com.dev.weixin.tools.models.response.VoiceRes;
import com.dev.weixin.tools.models.response.submodel.Article;
import com.dev.weixin.tools.models.response.submodel.Media;
import com.dev.weixin.tools.models.response.submodel.Video;
import com.dev.weixin.tools.utils.JAXBUtils;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author ��Ӣ
 * @see
 */
public class TestAll {

	@Test
	public void testJAXB() throws JAXBException {
		StringBuilder s = new StringBuilder();
		s.append("<xml><ToUserName><![CDATA[toUser]]></ToUserName>");
		s.append("<FromUserName><![CDATA[FromUser]]></FromUserName>");
		s.append("<CreateTime>123456789</CreateTime>");
		s.append("<MsgType><![CDATA[event]]></MsgType>");
		s.append("<Content><![CDATA[this is a test]]></Content>");
		s.append("<MsgId>1234567890123456</MsgId>");
//		s.append("<Event><![CDATA[subscribe]]></Event>");
//		s.append("<EventKey><![CDATA[qrscene_123123]]></EventKey>");
//		s.append("<Ticket><![CDATA[TICKET]]></Ticket>");
		s.append("</xml>");
//		ObjectMapper mapper = new ObjectMapper();
//		LinkedHashMap<String, Object> msg = JAXBUtils.fromXML(s.toString(), LinkedHashMap.class);
//		Assert.assertNotNull(msg);
//		System.out.println(msg.get("toUserName"));
//		System.out.println(msg.getToUserName());
		EventMsg event = JAXBUtils.fromXML(s.toString(), EventMsg.class);
		Assert.assertNotNull(event);
		Assert.assertNotNull(event.getFromUserName());
		System.out.println(event.getMsgtype());
		System.out.println(event.getClass());
		Media media = new Media();
		media.setMedia_id("dddd");
		VoiceRes res = new VoiceRes(media);
		System.out.println(JAXBUtils.toXML(res, "UTF-8"));
		
		Video video = new Video();
		video.setMediaId("cccc");
		video.setDescription("ddddddddd");
		VideoRes vres = new VideoRes(video);
		System.out.println(JAXBUtils.toXML(vres, "UTF-8"));
		
		List<Article> arts = new ArrayList<Article>();
		Article art = new Article();
		art.setDescription("dddd");
		arts.add(art);
		
		art = new Article();
		art.setDescription("dddd2");
		arts.add(art);
		NewsRes nres = new NewsRes(arts);
		System.out.println(JAXBUtils.toXML(nres, "UTF-8"));
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.setSerializationInclusion(Include.NON_NULL);  
			KFNewsMsg news = new KFNewsMsg();
			news.setNews(new News(arts));
			System.out.println(mapper.writeValueAsString(news));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
	}
}
