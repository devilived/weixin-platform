package com.dev.weixin.tools.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.customer.Customer;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.CustomerService;
import com.dev.weixin.tools.service.impl.BaseServiceImpl;
import com.dev.weixin.tools.service.impl.CustomerServiceImpl;

/**
 * @author 飘渺青衣
 * @see
 */
public class TestCustomerService {
	
	private BaseService service;
	
	private CustomerService customerService;
	
	@Before
	public void before() {
		WeixinConfig config = new WeixinConfig();
		config.setAppId("wx9a19b014440fdcd1");
		config.setAppSecret("506f39a2a7ba0bf255c1389b94f75af8");
		config.setToken("101010101");
		config.setEncodingAESKey("");
		service = new BaseServiceImpl(config);
		customerService = new CustomerServiceImpl(service);
	}
	
	@Test
	public void testAdd() throws ErrorMsgException {
		Customer customer = new Customer();
		customer.setKf_account("customer1@shujikeji");
		customer.setNickname("�ͷ�1");
		customer.setPassword("12345678");
		Assert.assertTrue(this.customerService.add(customer));
	}
	
	@Test
	public void testUpdate() throws ErrorMsgException {
		Customer customer = new Customer();
		customer.setKf_account("customer1@shujikeji");
		customer.setNickname("�ͷ�2");
		customer.setPassword("25d55ad283aa400af464c76d713c07ad");
		Assert.assertTrue(this.customerService.update(customer));
	}
	
	@Test
	public void testRemove() throws ErrorMsgException {
		Customer customer = new Customer();
		customer.setKf_account("customer1@shujikeji");
		customer.setNickname("�ͷ�2");
		customer.setPassword("25d55ad283aa400af464c76d713c07ad");
		Assert.assertTrue(this.customerService.remove(customer));
	}
	
	@Test
	public void testGet() throws ErrorMsgException {
		List<Customer> list = this.customerService.getKfList();
		Assert.assertNotNull(list);
//		Assert.assertEquals(list.size(), 0);
	}
}
