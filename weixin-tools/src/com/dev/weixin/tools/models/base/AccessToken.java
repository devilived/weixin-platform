package com.dev.weixin.tools.models.base;

/**
 * 获取access token
 * @author 飘渺青衣
 * @see
 */
public class AccessToken {

	/**
	 * 获取到的凭证
	 */
	private String access_token;
	
	/**
	 * 凭证有效时间，单位：秒
	 */
	private long expires_in;
	
	/**
	 * 凭证获得时间
	 */
	private long time_in = System.currentTimeMillis();

	/**
	 * 是否超时
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public boolean isTimeout() {
		long delay = System.currentTimeMillis() - this.time_in + 5000;
		return delay > this.time_in * 1000;
	}
	
	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public long getTime_in() {
		return time_in;
	}

	public void setTime_in(long time_in) {
		this.time_in = time_in;
	}
	
}
