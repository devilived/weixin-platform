package com.dev.weixin.tools.models.base;

/**
 * 错误信息
 * @author 飘渺青衣
 * @see
 */
public class ErrorMsg {

	private String errcode;
	
	private String errmsg;

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	
}
