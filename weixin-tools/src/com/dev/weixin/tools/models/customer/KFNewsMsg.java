package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.customer.submodel.News;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFNewsMsg extends BaseMsg {
	
	private News news;
	
	private Customer customservice;

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}
	
}
