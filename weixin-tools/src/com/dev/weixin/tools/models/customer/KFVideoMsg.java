package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.response.VideoRes;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFVideoMsg extends VideoRes {
	
	private Customer customservice;

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}
}
