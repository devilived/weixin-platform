package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.response.ImageRes;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFImageMsg extends ImageRes {
	
	private Customer customservice;

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}
}
