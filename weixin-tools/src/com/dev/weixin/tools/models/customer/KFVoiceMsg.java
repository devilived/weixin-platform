package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.response.VoiceRes;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFVoiceMsg extends VoiceRes {
	
	private Customer customservice;

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}
}
