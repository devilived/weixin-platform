package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.customer.submodel.Text;

/**
 * 返回文本信息json
 * @author 飘渺青衣
 * @see
 */
public class KFTextMsg extends BaseMsg {

	private Text text;
	
	private Customer customservice;

	public KFTextMsg() {
	}
	
	public KFTextMsg(String content) {
		this.setText(new Text(content));
	}
	
	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}
	
}
