package com.dev.weixin.tools.models.customer.submodel;

import java.util.List;

import com.dev.weixin.tools.models.response.submodel.Article;

/**
 * @author 飘渺青衣
 * @see
 */
public class News {

	private List<Article> articles;

	public News() {}
	
	public News(List<Article> articles) {
		this.setArticles(articles);
	}
	
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
}
