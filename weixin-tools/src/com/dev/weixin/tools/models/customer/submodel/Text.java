package com.dev.weixin.tools.models.customer.submodel;

/**
 * @author 飘渺青衣
 * @see
 */
public class Text {

	private String content;

	public Text() {}
	
	public Text(String content) {
		this.setContent(content);
	}
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
