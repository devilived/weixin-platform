package com.dev.weixin.tools.models.customer;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFList {

	private List<Customer> kf_list;

	public List<Customer> getKf_list() {
		return kf_list;
	}

	public void setKf_list(List<Customer> kf_list) {
		this.kf_list = kf_list;
	}
	
}
