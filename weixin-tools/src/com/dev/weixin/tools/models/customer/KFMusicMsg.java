package com.dev.weixin.tools.models.customer;

import com.dev.weixin.tools.models.response.MusicRes;

/**
 * @author 飘渺青衣
 * @see
 */
public class KFMusicMsg extends MusicRes {
	
	private Customer customservice;

	public Customer getCustomservice() {
		return customservice;
	}

	public void setCustomservice(Customer customservice) {
		this.customservice = customservice;
	}
}
