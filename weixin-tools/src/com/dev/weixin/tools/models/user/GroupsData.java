package com.dev.weixin.tools.models.user;

import java.util.List;

/**
 * 用户分组列表信息
 * @author 飘渺青衣
 * @see
 */
public class GroupsData {

	private List<Group> groups;

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
}
