package com.dev.weixin.tools.models.user;

/**
 * 数据信息
 * @author 飘渺青衣
 * @see
 */
public class GroupData {

	private Group group;

	public GroupData() {}
	
	public GroupData(Group group) {
		this.group = group;
	}
	
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}
	
}
