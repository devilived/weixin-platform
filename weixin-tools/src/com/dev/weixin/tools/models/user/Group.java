package com.dev.weixin.tools.models.user;

/**
 * 用户分组信息
 * @author 飘渺青衣
 * @see
 */
public class Group {

	private String id;
	
	private String name;
	
	private Integer count;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
}
