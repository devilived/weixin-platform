package com.dev.weixin.tools.models.user;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class Data {

	private List<String> openid;
	
	public List<String> getOpenid() {
		return openid;
	}

	public void setOpenid(List<String> openid) {
		this.openid = openid;
	}
}
