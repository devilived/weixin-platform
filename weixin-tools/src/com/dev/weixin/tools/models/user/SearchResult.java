package com.dev.weixin.tools.models.user;

/**
 * @author 飘渺青衣
 * @see
 */
public class SearchResult {

	private String groupid;

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	
}
