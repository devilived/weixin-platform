package com.dev.weixin.tools.models.user;


/**
 * @author 飘渺青衣
 * @see
 */
public class UserList {

	private long total;
	
	private int count;
	
	private Data data;
	
	private String next_openid;
	
//	/**
//	 * 是否还有用户
//	 * @author 飘渺青衣
//	 * @version
//	 * @return
//	 */
//	public boolean hasNext() {
//		return data != null ? !StringUtils.equals(data.getOpenid().get(data.getOpenid().size() - 1), this.next_openid) : false;
//	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getNext_openid() {
		return next_openid;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}
}
