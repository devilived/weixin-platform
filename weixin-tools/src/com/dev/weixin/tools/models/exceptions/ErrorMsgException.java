package com.dev.weixin.tools.models.exceptions;

import com.dev.weixin.tools.models.base.ErrorMsg;

/**
 * 接口返回错误异常
 * @author 飘渺青衣
 * @see
 */
public class ErrorMsgException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7396091779262308307L;

	private ErrorMsg error;
	
	public ErrorMsgException(ErrorMsg error, String message) {
		super(message);
		this.error = error;
	}
	
	public ErrorMsgException(ErrorMsg error) {
		super("{errorcode:" + error.getErrcode()+ ", errormsg:\""+ error.getErrmsg() + "\"}");
		this.error = error;
	}
	
	public ErrorMsg getError() {
		return error;
	}
	
}
