package com.dev.weixin.tools.models.statistic;

/**
 * 获取用户增减数据
 * @author 飘渺青衣
 * @see
 */
public class Decrease {

	private String ref_date;
	
	private long user_source;
	
	private int new_user;
	
	private int cancel_user;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public long getUser_source() {
		return user_source;
	}

	public void setUser_source(long user_source) {
		this.user_source = user_source;
	}

	public int getNew_user() {
		return new_user;
	}

	public void setNew_user(int new_user) {
		this.new_user = new_user;
	}

	public int getCancel_user() {
		return cancel_user;
	}

	public void setCancel_user(int cancel_user) {
		this.cancel_user = cancel_user;
	}
	
}
