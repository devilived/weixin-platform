package com.dev.weixin.tools.models.statistic;

/**
 * @author 飘渺青衣
 * @see
 */
public class ArticleTotalDetail {

	private String stat_date;
	
	private long target_user;
	
	private int int_page_read_user;
	
	private int int_page_read_count;
	
	private int ori_page_read_user;
	
	private int ori_page_read_count;
	
	private int share_user;
	
	private int share_count;
	
	private int add_to_fav_user;
	
	private int add_to_fav_count;

	public String getStat_date() {
		return stat_date;
	}

	public void setStat_date(String stat_date) {
		this.stat_date = stat_date;
	}

	public long getTarget_user() {
		return target_user;
	}

	public void setTarget_user(long target_user) {
		this.target_user = target_user;
	}

	public int getInt_page_read_user() {
		return int_page_read_user;
	}

	public void setInt_page_read_user(int int_page_read_user) {
		this.int_page_read_user = int_page_read_user;
	}

	public int getInt_page_read_count() {
		return int_page_read_count;
	}

	public void setInt_page_read_count(int int_page_read_count) {
		this.int_page_read_count = int_page_read_count;
	}

	public int getOri_page_read_user() {
		return ori_page_read_user;
	}

	public void setOri_page_read_user(int ori_page_read_user) {
		this.ori_page_read_user = ori_page_read_user;
	}

	public int getOri_page_read_count() {
		return ori_page_read_count;
	}

	public void setOri_page_read_count(int ori_page_read_count) {
		this.ori_page_read_count = ori_page_read_count;
	}

	public int getShare_user() {
		return share_user;
	}

	public void setShare_user(int share_user) {
		this.share_user = share_user;
	}

	public int getShare_count() {
		return share_count;
	}

	public void setShare_count(int share_count) {
		this.share_count = share_count;
	}

	public int getAdd_to_fav_user() {
		return add_to_fav_user;
	}

	public void setAdd_to_fav_user(int add_to_fav_user) {
		this.add_to_fav_user = add_to_fav_user;
	}

	public int getAdd_to_fav_count() {
		return add_to_fav_count;
	}

	public void setAdd_to_fav_count(int add_to_fav_count) {
		this.add_to_fav_count = add_to_fav_count;
	}
}
