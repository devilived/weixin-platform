package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class ArticleSummaryList {

	private List<ArticleSummary> list;

	public List<ArticleSummary> getList() {
		return list;
	}

	public void setList(List<ArticleSummary> list) {
		this.list = list;
	}
}
