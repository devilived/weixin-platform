package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class UpStreamMsgDistList {

	private List<UpStreamMsgDist> list;

	public List<UpStreamMsgDist> getList() {
		return list;
	}

	public void setList(List<UpStreamMsgDist> list) {
		this.list = list;
	}
	
}
