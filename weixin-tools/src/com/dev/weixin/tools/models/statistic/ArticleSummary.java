package com.dev.weixin.tools.models.statistic;

/**
 * 获取图文群发每日数据
 * @author 飘渺青衣
 * @see
 */
public class ArticleSummary {

	private String ref_date;
	
	private String msgid;
	
	private String title;
	
	private int ref_hour;
	
	private int int_page_read_user;
	
	private int int_page_read_count;
	
	private int ori_page_read_user;
	
	private int ori_page_read_count;
	
	private int share_scene;
	
	private int share_user;
	
	private int share_count;
	
	private int add_to_fav_user;
	
	private int add_to_fav_count;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getRef_hour() {
		return ref_hour;
	}

	public void setRef_hour(int ref_hour) {
		this.ref_hour = ref_hour;
	}

	public int getInt_page_read_user() {
		return int_page_read_user;
	}

	public void setInt_page_read_user(int int_page_read_user) {
		this.int_page_read_user = int_page_read_user;
	}

	public int getInt_page_read_count() {
		return int_page_read_count;
	}

	public void setInt_page_read_count(int int_page_read_count) {
		this.int_page_read_count = int_page_read_count;
	}

	public int getOri_page_read_user() {
		return ori_page_read_user;
	}

	public void setOri_page_read_user(int ori_page_read_user) {
		this.ori_page_read_user = ori_page_read_user;
	}

	public int getOri_page_read_count() {
		return ori_page_read_count;
	}

	public void setOri_page_read_count(int ori_page_read_count) {
		this.ori_page_read_count = ori_page_read_count;
	}

	public int getShare_scene() {
		return share_scene;
	}

	public void setShare_scene(int share_scene) {
		this.share_scene = share_scene;
	}

	public int getShare_user() {
		return share_user;
	}

	public void setShare_user(int share_user) {
		this.share_user = share_user;
	}

	public int getShare_count() {
		return share_count;
	}

	public void setShare_count(int share_count) {
		this.share_count = share_count;
	}

	public int getAdd_to_fav_user() {
		return add_to_fav_user;
	}

	public void setAdd_to_fav_user(int add_to_fav_user) {
		this.add_to_fav_user = add_to_fav_user;
	}

	public int getAdd_to_fav_count() {
		return add_to_fav_count;
	}

	public void setAdd_to_fav_count(int add_to_fav_count) {
		this.add_to_fav_count = add_to_fav_count;
	}
	
	
}
