package com.dev.weixin.tools.models.statistic;

/**
 * 消息发送分布数据
 * @author 飘渺青衣
 * @see
 */
public class UpStreamMsgDist {

	private String ref_date;
	
	private int count_interval;
	
	private int msg_user;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public int getCount_interval() {
		return count_interval;
	}

	public void setCount_interval(int count_interval) {
		this.count_interval = count_interval;
	}

	public int getMsg_user() {
		return msg_user;
	}

	public void setMsg_user(int msg_user) {
		this.msg_user = msg_user;
	}
}
