package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class DecreaseList {

	private List<Decrease> list;

	public List<Decrease> getList() {
		return list;
	}

	public void setList(List<Decrease> list) {
		this.list = list;
	}
}
