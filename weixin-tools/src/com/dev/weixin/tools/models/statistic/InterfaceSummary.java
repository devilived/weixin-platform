package com.dev.weixin.tools.models.statistic;

/**
 * 接口分析数据
 * @author 飘渺青衣
 * @see
 */
public class InterfaceSummary {

	private String ref_date;
	
	private int ref_hour;
	
	private int callback_count;
	
	private int fail_count;
	
	private long total_time_cost;
	
	private int max_time_cost;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public int getRef_hour() {
		return ref_hour;
	}

	public void setRef_hour(int ref_hour) {
		this.ref_hour = ref_hour;
	}

	public int getCallback_count() {
		return callback_count;
	}

	public void setCallback_count(int callback_count) {
		this.callback_count = callback_count;
	}

	public int getFail_count() {
		return fail_count;
	}

	public void setFail_count(int fail_count) {
		this.fail_count = fail_count;
	}

	public long getTotal_time_cost() {
		return total_time_cost;
	}

	public void setTotal_time_cost(long total_time_cost) {
		this.total_time_cost = total_time_cost;
	}

	public int getMax_time_cost() {
		return max_time_cost;
	}

	public void setMax_time_cost(int max_time_cost) {
		this.max_time_cost = max_time_cost;
	}
}
