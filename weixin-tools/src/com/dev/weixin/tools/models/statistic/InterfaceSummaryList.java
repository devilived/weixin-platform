package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class InterfaceSummaryList {

	private List<InterfaceSummary> list;

	public List<InterfaceSummary> getList() {
		return list;
	}

	public void setList(List<InterfaceSummary> list) {
		this.list = list;
	}
}
