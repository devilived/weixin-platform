package com.dev.weixin.tools.models.statistic;

/**
 * 获取消息发送概况数据
 * @author 飘渺青衣
 * @see
 */
public class UpStreamMsg {

	private String ref_date;
	
	private int ref_hour;
	
	private String msg_type;
	
	private int msg_user;
	
	private int msg_count;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public int getRef_hour() {
		return ref_hour;
	}

	public void setRef_hour(int ref_hour) {
		this.ref_hour = ref_hour;
	}

	public String getMsg_type() {
		return msg_type;
	}

	public void setMsg_type(String msg_type) {
		this.msg_type = msg_type;
	}

	public int getMsg_user() {
		return msg_user;
	}

	public void setMsg_user(int msg_user) {
		this.msg_user = msg_user;
	}

	public int getMsg_count() {
		return msg_count;
	}

	public void setMsg_count(int msg_count) {
		this.msg_count = msg_count;
	}
}
