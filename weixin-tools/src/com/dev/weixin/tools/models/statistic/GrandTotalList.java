package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class GrandTotalList {

	private List<GrandTotal> list;

	public List<GrandTotal> getList() {
		return list;
	}

	public void setList(List<GrandTotal> list) {
		this.list = list;
	}
	
}
