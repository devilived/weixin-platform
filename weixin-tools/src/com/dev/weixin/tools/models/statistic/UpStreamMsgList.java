package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * @author 飘渺青衣
 * @see
 */
public class UpStreamMsgList {

	private List<UpStreamMsg> list;

	public List<UpStreamMsg> getList() {
		return list;
	}

	public void setList(List<UpStreamMsg> list) {
		this.list = list;
	}
}
