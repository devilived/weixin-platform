package com.dev.weixin.tools.models.statistic;

import java.util.List;

/**
 * 图文群发总数据
 * @author 飘渺青衣
 * @see
 */
public class ArticleTotal {

	private String ref_date;
	
	private String msgid;
	
	private String title;
	
	private List<ArticleTotalDetail> details;

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<ArticleTotalDetail> getDetails() {
		return details;
	}

	public void setDetails(List<ArticleTotalDetail> details) {
		this.details = details;
	}
}
