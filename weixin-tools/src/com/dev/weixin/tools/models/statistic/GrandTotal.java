package com.dev.weixin.tools.models.statistic;

/**
 * 获取累计用户数据
 * @author 飘渺青衣
 * @see
 */
public class GrandTotal {

	private String ref_date;
	
	private long user_source;
	
	private long cumulate_user;

	public long getUser_source() {
		return user_source;
	}

	public void setUser_source(long user_source) {
		this.user_source = user_source;
	}

	public String getRef_date() {
		return ref_date;
	}

	public void setRef_date(String ref_date) {
		this.ref_date = ref_date;
	}

	public long getCumulate_user() {
		return cumulate_user;
	}

	public void setCumulate_user(long cumulate_user) {
		this.cumulate_user = cumulate_user;
	}
}
