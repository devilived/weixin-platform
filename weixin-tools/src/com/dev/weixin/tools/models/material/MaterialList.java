package com.dev.weixin.tools.models.material;

import java.util.List;

/**
 * 素材列表
 * @author 飘渺青衣
 * @see
 */
public class MaterialList {

	private int total_count;
	
	private int item_count;
	
	private List<MaterialItem> item;

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public int getItem_count() {
		return item_count;
	}

	public void setItem_count(int item_count) {
		this.item_count = item_count;
	}

	public List<MaterialItem> getItem() {
		return item;
	}

	public void setItem(List<MaterialItem> item) {
		this.item = item;
	}
}
