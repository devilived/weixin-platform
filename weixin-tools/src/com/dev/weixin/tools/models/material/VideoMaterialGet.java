package com.dev.weixin.tools.models.material;

/**
 * 频消息素材
 * @author 飘渺青衣
 * @see
 */
public class VideoMaterialGet {

	private String media_id;
	
	private String title;
	
	private String description;
	
	private String down_url;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDown_url() {
		return down_url;
	}

	public void setDown_url(String down_url) {
		this.down_url = down_url;
	}
	
}
