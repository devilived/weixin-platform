package com.dev.weixin.tools.models.material;

/**
 * @author 飘渺青衣
 * @see
 */
public class VideoMaterial {

	private String media_id;
	
	private String type;
	
	private String title;
	
	private String introduction;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
}
