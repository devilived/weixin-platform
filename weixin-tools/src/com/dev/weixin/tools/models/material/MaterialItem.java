package com.dev.weixin.tools.models.material;

/**
 * @author 飘渺青衣
 * @see
 */
public class MaterialItem {

	private String media_id;
	
	private String name;
	
	private String update_time;
	
	private GraphicsTextMaterials content;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GraphicsTextMaterials getContent() {
		return content;
	}

	public void setContent(GraphicsTextMaterials content) {
		this.content = content;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
}
