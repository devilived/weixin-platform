package com.dev.weixin.tools.models.material;

import java.util.List;

/**
 * 图文永久素材
 * @author 飘渺青衣
 * @see
 */
public class GraphicsTextMaterials {

	private String media_id;
	
	private Integer index;
	
	private List<GraphicsTextMaterial> articles;
	
	private List<GraphicsTextMaterial> news_item;

	public GraphicsTextMaterials() {}
	
	public GraphicsTextMaterials(List<GraphicsTextMaterial> articles) {
		this.articles = articles;
	}
	
	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public List<GraphicsTextMaterial> getArticles() {
		return articles;
	}

	public void setArticles(List<GraphicsTextMaterial> articles) {
		this.articles = articles;
	}

	public List<GraphicsTextMaterial> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<GraphicsTextMaterial> news_item) {
		this.news_item = news_item;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
}
