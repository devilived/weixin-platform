package com.dev.weixin.tools.models.account;

import com.dev.weixin.tools.models.base.ErrorMsg;

/**
 * 短链接
 * @author 飘渺青衣
 * @see
 */
public class ShortUrl extends ErrorMsg {

	private String short_url;

	public String getShort_url() {
		return short_url;
	}

	public void setShort_url(String short_url) {
		this.short_url = short_url;
	}
}
