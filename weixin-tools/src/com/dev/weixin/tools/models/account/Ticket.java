package com.dev.weixin.tools.models.account;

/**
 * 生成二维码信息
 * @author 飘渺青衣
 * @see
 */
public class Ticket {

	private String ticket;
	
	private int expire_seconds;
	
	private long current_time = System.nanoTime();
	
	private String url;
	
	/**
	 * 是否超时 expire_seconds>0时判定
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public boolean isTimeout() {
		if(this.expire_seconds > 0) {
			long time = (System.nanoTime() - current_time) / 1000;
			return time > this.expire_seconds;
		}
		return false;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public int getExpire_seconds() {
		return expire_seconds;
	}

	public void setExpire_seconds(int expire_seconds) {
		this.expire_seconds = expire_seconds;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
