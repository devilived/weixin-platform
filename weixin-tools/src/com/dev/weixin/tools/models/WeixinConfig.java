package com.dev.weixin.tools.models;

import com.dev.weixin.tools.models.base.AccessToken;


/**
 * 微信接口基础信息
 * @author 飘渺青衣
 * @see
 */
public class WeixinConfig {

	public static final String ENCODING = "UTF-8";
	
	private String appId;
	
	private String appSecret;
	
	private String token;
	
	private String encodingAESKey;
	
	private AccessToken accessToken;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEncodingAESKey() {
		return encodingAESKey;
	}

	public void setEncodingAESKey(String encodingAESKey) {
		this.encodingAESKey = encodingAESKey;
	}

	public AccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(AccessToken accessToken) {
		this.accessToken = accessToken;
	}
	
}
