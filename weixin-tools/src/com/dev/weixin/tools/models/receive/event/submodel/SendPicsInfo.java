package com.dev.weixin.tools.models.receive.event.submodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SendPicsInfo {
	
	@XmlElement(name = "Count")
	private int count;
	
	@XmlElement(name = "PicList")
	private PicList picList;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public PicList getPicList() {
		return picList;
	}

	public void setPicList(PicList picList) {
		this.picList = picList;
	}
}
