package com.dev.weixin.tools.models.receive.event;

import javax.xml.bind.annotation.XmlElement;

import com.dev.weixin.tools.models.receive.EventMsg;
import com.dev.weixin.tools.models.receive.event.submodel.SendLocationInfo;

/**
 * 弹出地理位置选择器的事件推送
 * @author 飘渺青衣
 * @see
 */
public class LocationSelectEvent extends EventMsg {
	
	@XmlElement(name = "EventKey")
	private String eventKey;
	
	@XmlElement(name = "SendLocationInfo")
	private SendLocationInfo sendLocationInfo;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public SendLocationInfo getSendLocationInfo() {
		return sendLocationInfo;
	}

	public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
		this.sendLocationInfo = sendLocationInfo;
	}
}
