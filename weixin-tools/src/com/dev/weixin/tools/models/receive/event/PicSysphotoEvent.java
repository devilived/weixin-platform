package com.dev.weixin.tools.models.receive.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.receive.EventMsg;
import com.dev.weixin.tools.models.receive.event.submodel.SendPicsInfo;

/**
 * 弹出系统拍照发图的事件推送
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class PicSysphotoEvent extends EventMsg {
	
	@XmlElement(name = "EventKey")
	private String eventKey;
	
	@XmlElement(name = "SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}
}
