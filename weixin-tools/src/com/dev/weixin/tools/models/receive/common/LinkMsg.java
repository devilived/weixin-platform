package com.dev.weixin.tools.models.receive.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;

/**
 * 链接消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class LinkMsg extends BaseMsg {
	
	@XmlElement(name = "Title")
	private String title;
	
	@XmlElement(name = "Description")
	private String tescription;
	
	@XmlElement(name = "Url")
	private String url;
	
	@XmlElement(name = "MsgId")
	private String msgId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTescription() {
		return tescription;
	}

	public void setTescription(String tescription) {
		this.tescription = tescription;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
}
