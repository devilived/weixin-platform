package com.dev.weixin.tools.models.receive;

import org.apache.commons.lang3.StringUtils;

/**
 * 校验信息
 * @author 飘渺青衣
 * @see
 */
public class ValidData {

	/**
	 * 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
	 */
	private String signature;
	
	/**
	 * 时间戳
	 */
	private String timestamp;
	
	/**
	 * 随机数
	 */
	private String nonce;
	
	/**
	 * 随机字符串
	 */
	private String echostr;
	
	/**
	 * 表示加密类型 raw不加密 aes：aes加密
	 */
	private String encrypt_type;
	
	/**
	 * 表示对消息体的签名
	 */
	private String msg_signature;
	
	/**
	 * 是否AES加密
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public boolean isEncrypt() {
		return !StringUtils.isEmpty(this.encrypt_type) ? StringUtils.equals("aes", this.encrypt_type) : false;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}

	public String getEchostr() {
		return echostr;
	}

	public void setEchostr(String echostr) {
		this.echostr = echostr;
	}

	public String getEncrypt_type() {
		return encrypt_type;
	}

	public void setEncrypt_type(String encrypt_type) {
		this.encrypt_type = encrypt_type;
	}

	public String getMsg_signature() {
		return msg_signature;
	}

	public void setMsg_signature(String msg_signature) {
		this.msg_signature = msg_signature;
	}
	
}
