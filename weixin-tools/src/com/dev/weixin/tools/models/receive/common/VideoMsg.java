package com.dev.weixin.tools.models.receive.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;

/**
 * 视频和小视频消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class VideoMsg extends BaseMsg {
	
	@XmlElement(name = "MediaId")
	private String mediaId;
	
	@XmlElement(name = "ThumbMediaId")
	private String thumbMediaId;
	
	@XmlElement(name = "MsgId")
	private String msgId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}
