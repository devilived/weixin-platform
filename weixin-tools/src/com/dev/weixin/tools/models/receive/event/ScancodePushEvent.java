package com.dev.weixin.tools.models.receive.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.receive.EventMsg;
import com.dev.weixin.tools.models.receive.event.submodel.ScanCodeInfo;

/**
 * 扫码推事件的事件推送
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScancodePushEvent extends EventMsg {
	
	@XmlElement(name = "EventKey")
	private String eventKey;
	
	@XmlElement(name = "ScanCodeInfo")
	private ScanCodeInfo scanCodeInfo;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public ScanCodeInfo getScanCodeInfo() {
		return scanCodeInfo;
	}

	public void setScanCodeInfo(ScanCodeInfo scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}
	
}
