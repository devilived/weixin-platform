package com.dev.weixin.tools.models.receive.event.submodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Item {

	@XmlElement(name = "PicMd5Sum")
	private String picMd5Sum;

	public String getPicMd5Sum() {
		return picMd5Sum;
	}

	public void setPicMd5Sum(String picMd5Sum) {
		this.picMd5Sum = picMd5Sum;
	}
	
}
