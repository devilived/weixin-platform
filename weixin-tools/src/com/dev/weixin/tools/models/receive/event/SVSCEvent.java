package com.dev.weixin.tools.models.receive.event;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.receive.EventMsg;

/**
 * 扫描带参数二维码事件、用户已关注时的事件推送、自定义菜单事件、点击菜单跳转链接时的事件推送
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class SVSCEvent extends EventMsg {

	@XmlElement(name = "EventKey")
	private String eventKey;
	
	@XmlElement(name = "Ticket")
	private String ticket;

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
}
