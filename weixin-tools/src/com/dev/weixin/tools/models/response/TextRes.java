package com.dev.weixin.tools.models.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;

/**
 * 回复文本消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class TextRes extends BaseMsg {
	
	@XmlElement(name = "Content")
	private String content;
	
	public TextRes() {
		this.setMsgtype("text");
	}
	
	public TextRes(String content) {
		this();
		this.setContent(content);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
