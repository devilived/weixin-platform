package com.dev.weixin.tools.models.response.submodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Music {
	
	@XmlElement(name = "Title")
	private String title;
	
	@XmlElement(name = "Description")
	private String description;
	
	@XmlElement(name = "MusicUrl")
	private String musicurl;
	
	@XmlElement(name = "HQMusicUrl")
	private String hqmusicurl;
	
	@XmlElement(name = "ThumbMediaId")
	private String thumb_media_id;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMusicurl() {
		return musicurl;
	}

	public void setMusicurl(String musicurl) {
		this.musicurl = musicurl;
	}

	public String getHqmusicurl() {
		return hqmusicurl;
	}

	public void setHqmusicurl(String hqmusicurl) {
		this.hqmusicurl = hqmusicurl;
	}

	public String getThumb_media_id() {
		return thumb_media_id;
	}

	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}

}
