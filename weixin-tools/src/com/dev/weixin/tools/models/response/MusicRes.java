package com.dev.weixin.tools.models.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.submodel.Music;

/**
 * 回复音乐消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class MusicRes extends BaseMsg {
	
	@XmlElement(name = "Music")
	private Music music;
	
	public MusicRes() {
		this.setMsgtype("music");
	}
	
	public MusicRes(Music music) {
		this();
		this.setMusic(music);
	}

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}
	
}
