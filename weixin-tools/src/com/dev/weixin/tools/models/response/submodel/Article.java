package com.dev.weixin.tools.models.response.submodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Article {
	
	@XmlElement(name = "Title")
	private String title;
	
	@XmlElement(name = "Description")
	private String description;
	
	@XmlElement(name = "PicUrl")
	private String picurl;
	
	@XmlElement(name = "Url")
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPicurl() {
		return picurl;
	}

	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
}
