package com.dev.weixin.tools.models.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.submodel.Article;
import com.dev.weixin.tools.models.response.submodel.Articles;

/**
 * 回复图文消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class NewsRes extends BaseMsg {

	@XmlElement(name = "ArticleCount")
	private int articleCount;
	
	@XmlElement(name = "Articles")
	private Articles news;

	public NewsRes() {
		this.setMsgtype("news");
	}
	
	public NewsRes(List<Article> articles) {
		this();
		this.setArticles(articles);
	}
	
	public int getArticleCount() {
		return articleCount;
	}

	public void setArticleCount(int articleCount) {
		this.articleCount = articleCount;
	}

	public List<Article> getArticles() {
		return news.getItem();
	}

	public void setArticles(List<Article> articles) {
		this.news = new Articles(articles);
		this.setArticleCount(articles.size());
	}

	public Articles getNews() {
		return news;
	}

	public void setNews(Articles news) {
		this.news = news;
	}
	
}
