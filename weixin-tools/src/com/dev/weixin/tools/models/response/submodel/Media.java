package com.dev.weixin.tools.models.response.submodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Media {

	@XmlElement(name = "MediaId")
	private String media_id;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	
}
