package com.dev.weixin.tools.models.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.submodel.Media;

/**
 * 回复图片消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageRes extends BaseMsg {
	
	@XmlElement(name = "Image")
	private Media image;
	
	public ImageRes() {
		this.setMsgtype("image");
	}
	
	public ImageRes(Media image) {
		this();
		this.setImage(image);
	}
	
	public Media getImage() {
		return image;
	}

	public void setImage(Media image) {
		this.image = image;
	}
	
}
