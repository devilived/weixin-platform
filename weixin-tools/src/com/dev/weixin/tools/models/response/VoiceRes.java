package com.dev.weixin.tools.models.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.submodel.Media;

/**
 * 回复语音消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class VoiceRes extends BaseMsg {
	
	@XmlElement(name = "Voice")
	private Media voice;
	
	public VoiceRes() {
		this.setMsgtype("voice");
	}
	
	public VoiceRes(Media voice) {
		this();
		this.setVoice(voice);
	}

	public Media getVoice() {
		return voice;
	}

	public void setVoice(Media voice) {
		this.voice = voice;
	}
}
