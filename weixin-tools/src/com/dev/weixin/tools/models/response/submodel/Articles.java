package com.dev.weixin.tools.models.response.submodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author 飘渺青衣
 * @see
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Articles {
	
	@XmlElement(name = "Item")
	private List<Article> item;

	public Articles(List<Article> item) {
		this.item = item;
	}
	
	public List<Article> getItem() {
		return item;
	}

	public void setItem(List<Article> item) {
		this.item = item;
	}
	
}
