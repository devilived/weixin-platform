package com.dev.weixin.tools.models.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.response.submodel.Video;

/**
 * 回复视频消息
 * @author 飘渺青衣
 * @see
 */
@XmlRootElement(name = "xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class VideoRes extends BaseMsg {

	@XmlElement(name = "Video")
	private Video video;
	
	public VideoRes() {
		this.setMsgtype("video");
	}
	
	public VideoRes(Video video) {
		this();
		this.setVideo(video);
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
}
