package com.dev.weixin.tools.models.menu;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义菜单
 * @author 飘渺青衣
 * @see
 */
public class Menu {

	private List<Button> button = new ArrayList<Button>();

	public List<Button> getButton() {
		return button;
	}

	public void setButton(List<Button> button) {
		this.button = button;
	}
}
