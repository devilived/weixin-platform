package com.dev.weixin.tools.models.menu;

/**
 * @author 飘渺青衣
 * @see
 */
public class GetMenu {

	private Menu menu;

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
}
