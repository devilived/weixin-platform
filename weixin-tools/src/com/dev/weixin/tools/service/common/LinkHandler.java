package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.LinkMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 链接消息接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface LinkHandler extends ReceiveHandler<LinkMsg> {

}
