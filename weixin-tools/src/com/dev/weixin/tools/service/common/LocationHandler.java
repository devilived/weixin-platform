package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.LocationMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 地理位置消息接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface LocationHandler extends ReceiveHandler<LocationMsg> {

}
