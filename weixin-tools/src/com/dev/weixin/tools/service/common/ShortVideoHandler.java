package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.VideoMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 小视频消息接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface ShortVideoHandler extends ReceiveHandler<VideoMsg> {

}
