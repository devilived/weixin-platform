package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.ImageMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 图片消息接收事件接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface ImageHandler extends ReceiveHandler<ImageMsg> {

}
