package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.VoiceMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 语音消息接收事件接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface VoiceHandler extends ReceiveHandler<VoiceMsg> {

}
