package com.dev.weixin.tools.service.common;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.common.TextMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 文本信息接收事件接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface TextHandler extends ReceiveHandler<TextMsg> {

}
