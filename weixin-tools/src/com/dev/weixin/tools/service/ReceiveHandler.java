package com.dev.weixin.tools.service;

import com.dev.weixin.tools.models.BaseMsg;

/**
 * 接收事件
 * @author 飘渺青衣
 * @see
 */
public interface ReceiveHandler<T extends BaseMsg> {

	/**
	 * 接收事件触发
	 * @author 飘渺青衣
	 * @version
	 * @param msg
	 * @return
	 */
	public BaseMsg handler(T msg);
	
}
