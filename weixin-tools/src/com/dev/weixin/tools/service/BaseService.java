package com.dev.weixin.tools.service;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.base.IpAddress;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;

/**
 * 基础接口
 * @author 飘渺青衣
 * @see
 */
public interface BaseService {

	/**
	 * 获取access token
	 * @author 飘渺青衣
	 * @version
	 * @return
	 * @throws ErrorMsgException
	 */
	public String getAccessToken() throws ErrorMsgException;
	
	/**
	 * 获取微信服务器IP地址
	 * @author 飘渺青衣
	 * @version
	 * @return
	 * @throws ErrorMsgException
	 */
	public IpAddress getIpAddress() throws ErrorMsgException;
	
	/**
	 * 获取微信接口基础信息
	 * 使用的时候先调用setThreadLocalWeixinConfig或者getWeixinConfig
	 * 不然获取的配置文件都是默认的
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public WeixinConfig getWeixinConfig();
	
	/**
	 * 根据toUserName获取 微信配置，管理多个微信公众号时需要使用
	 * 记得线程结束时调用removeThreadLocalWeixinConfig()
	 * @author 飘渺青衣
	 * @version
	 * @param toUserName
	 * @return
	 * @throws ErrorMsgException 
	 */
	public WeixinConfig getWeixinConfig(String toUserName);
	
	/**
	 * 设置当前线程微信配置
	 * @author 飘渺青衣
	 * @version
	 * @param config
	 */
	public void setThreadLocalWeixinConfig(WeixinConfig config);
	
	/**
	 * 删除当前线程微信配置
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeThreadLocalWeixinConfig();
}
