package com.dev.weixin.tools.service;

import java.io.File;
import java.util.List;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.material.GraphicsTextMaterial;
import com.dev.weixin.tools.models.material.GraphicsTextMaterials;
import com.dev.weixin.tools.models.material.Material;
import com.dev.weixin.tools.models.material.MaterialCount;
import com.dev.weixin.tools.models.material.MaterialList;
import com.dev.weixin.tools.models.material.VideoMaterial;
import com.dev.weixin.tools.models.material.VideoMaterialGet;

/**
 * 输出微信接口
 * @author 飘渺青衣
 * @see
 */
public interface MaterialService {

	/**
	 * 新增临时素材
	 * @author 飘渺青衣
	 * @version
	 * @param type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @param file
	 * @return
	 * @throws ErrorMsgException 
	 */
	public Material addTempMaterial(String type, File file) throws ErrorMsgException;
	
	/**
	 * 
	 * @author 飘渺青衣
	 * @version
	 * @param media_id 
	 * @param filedir 保存文件的路径
	 * @return
	 * @throws ErrorMsgException
	 */
	public File getTempMaterial(String media_id, String filedir) throws ErrorMsgException;
	
	/**
	 * 新增永久图文素材
	 * @author 飘渺青衣
	 * @version
	 * @param materials 素材列表 media_id
	 * @return
	 * @throws ErrorMsgException 
	 */
	public GraphicsTextMaterials addNews(List<GraphicsTextMaterial> materials) throws ErrorMsgException;
	
	/**
	 * 修改永久图文素材
	 * @author 飘渺青衣
	 * @version
	 * @param materials
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean updateNews(GraphicsTextMaterials materials) throws ErrorMsgException;
	
	/**
	 * 新增其他类型永久素材
	 * @author 飘渺青衣
	 * @version
	 * @param type 媒体文件类型，分别有图片（image）、语音（voice）和缩略图（thumb）
	 * @param file
	 * @return
	 * @throws ErrorMsgException
	 */
	public Material addMaterial(String type, File file) throws ErrorMsgException;
	
	/**
	 * 新增其video类型永久素材
	 * @author 飘渺青衣
	 * @version
	 * @param title 视频素材的标题
	 * @param introduction 视频素材的描述
	 * @param file 视频文件
	 * @return
	 * @throws ErrorMsgException
	 */
	public VideoMaterial addVideoMaterial(String title, String introduction, File file) throws ErrorMsgException;
	
	/**
	 * 获取图文信息
	 * @author 飘渺青衣
	 * @version
	 * @param media_id
	 * @return
	 * @throws ErrorMsgException 
	 */
	public GraphicsTextMaterials getGraphicsTextMaterials(String media_id) throws ErrorMsgException;
	
	/**
	 * 获取视频信息
	 * @author 飘渺青衣
	 * @version
	 * @param media_id
	 * @return
	 * @throws ErrorMsgException
	 */
	public VideoMaterialGet getVideoMaterial(String media_id) throws ErrorMsgException;
	
	/**
	 * 获取其他永久素材
	 * @author 飘渺青衣
	 * @version
	 * @param media_id
	 * @param filepath 存储路径+文件名
	 * @return
	 * @throws ErrorMsgException
	 */
	public File getMaterial(String media_id, String filepath) throws ErrorMsgException;
	
	/**
	 * 删除永久素材
	 * @author 飘渺青衣
	 * @version
	 * @param media_id
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean removeMaterial(String media_id) throws ErrorMsgException;
	
	/**
	 * 素材总数
	 * @author 飘渺青衣
	 * @version
	 * @return
	 * @throws ErrorMsgException
	 */
	public MaterialCount getMaterialCount() throws ErrorMsgException;
	
	/**
	 * 获取素材列表
	 * @author 飘渺青衣
	 * @version
	 * @param type
	 * @param offset
	 * @param count
	 * @return
	 * @throws ErrorMsgException
	 */
	public MaterialList getMaterialList(String type, int offset, int count) throws ErrorMsgException;
}
