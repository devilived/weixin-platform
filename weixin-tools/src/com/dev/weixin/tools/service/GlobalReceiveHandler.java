package com.dev.weixin.tools.service;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.base.Handler;

/**
 * 全局接收信息接口（只有其他接收接口未返回信息才会执行此接口）
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface GlobalReceiveHandler extends ReceiveHandler<BaseMsg> {

}
