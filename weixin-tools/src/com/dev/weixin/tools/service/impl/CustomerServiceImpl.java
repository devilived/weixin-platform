package com.dev.weixin.tools.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.base.ErrorMsg;
import com.dev.weixin.tools.models.customer.Customer;
import com.dev.weixin.tools.models.customer.KFList;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.CustomerService;
import com.dev.weixin.tools.utils.URLConstants;
import com.dev.weixin.tools.utils.WeixinHttpClient;
import com.dev.weixin.tools.utils.md5.MD5;
import com.dev.weixin.tools.utils.policy.GetRequestPolicy;
import com.dev.weixin.tools.utils.policy.JsonRequestPolicy;
import com.dev.weixin.tools.utils.policy.RequestPolicy;
import com.dev.weixin.tools.utils.policy.UploadRequestPolicy;

/**
 * @author 飘渺青衣
 * @see
 */
@Service("weixinCustomerService")
public class CustomerServiceImpl implements CustomerService {
	
	private BaseService baseService;
	
	@Autowired
	public CustomerServiceImpl(BaseService baseService) {
		this.baseService = baseService;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#add(com.dev.weixin.tools.models.customer.Customer)
	 */
	@Override
	public boolean add(Customer customer) throws ErrorMsgException {
		customer.setPassword(MD5.getEncryption32(customer.getPassword()));
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		ErrorMsg error = WeixinHttpClient.request(new JsonRequestPolicy<ErrorMsg>(
				URLConstants.Customer.URL_KFACCOUNT_ADD, WeixinConfig.ENCODING,
				params, customer, ErrorMsg.class));
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#update(com.dev.weixin.tools.models.customer.Customer)
	 */
	@Override
	public boolean update(Customer customer) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		ErrorMsg error = WeixinHttpClient.request(new JsonRequestPolicy<ErrorMsg>(
				URLConstants.Customer.URL_KFACCOUNT_UPDATE, WeixinConfig.ENCODING,
				params, customer, ErrorMsg.class));
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#remove(com.dev.weixin.tools.models.customer.Customer)
	 */
	@Override
	public boolean remove(Customer customer) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		ErrorMsg error = WeixinHttpClient.request(new JsonRequestPolicy<ErrorMsg>(
				URLConstants.Customer.URL_KFACCOUNT_DEL, WeixinConfig.ENCODING,
				params, customer, ErrorMsg.class));
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#getKfList()
	 */
	@Override
	public List<Customer> getKfList() throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		RequestPolicy<KFList> policy = new GetRequestPolicy<KFList>(
				URLConstants.Customer.URL_KFACCOUNT_GETKFLIST,
				WeixinConfig.ENCODING,
				params, KFList.class);
		KFList kflist = WeixinHttpClient.request(policy);
		return kflist != null ? kflist.getKf_list() : null;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#send(com.dev.weixin.tools.models.BaseMsg)
	 */
	@Override
	public boolean send(BaseMsg msg) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		RequestPolicy<ErrorMsg> policy = new JsonRequestPolicy<ErrorMsg>(
				URLConstants.Customer.URL_KFACCOUNT_SEND,
				WeixinConfig.ENCODING,
				params, msg, ErrorMsg.class);
		ErrorMsg error = WeixinHttpClient.request(policy);
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.CustomerService#updateHeadImg(java.lang.String, java.io.File)
	 */
	@Override
	public boolean updateHeadImg(String kf_account, File file)
			throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		params.put("kf_account", kf_account);
		Map<String, Object> body = new LinkedHashMap<String, Object>();
		body.put("media", file);
		RequestPolicy<ErrorMsg> policy = new UploadRequestPolicy<ErrorMsg>(
				URLConstants.Customer.URL_KFACCOUNT_UPLOADHEADIMG,
				WeixinConfig.ENCODING,
				params, body, ErrorMsg.class);
		ErrorMsg error = WeixinHttpClient.request(policy);
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

}
