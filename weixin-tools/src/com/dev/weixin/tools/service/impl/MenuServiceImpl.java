package com.dev.weixin.tools.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.base.ErrorMsg;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.menu.GetMenu;
import com.dev.weixin.tools.models.menu.Menu;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.MenuService;
import com.dev.weixin.tools.utils.URLConstants;
import com.dev.weixin.tools.utils.WeixinHttpClient;
import com.dev.weixin.tools.utils.policy.GetRequestPolicy;
import com.dev.weixin.tools.utils.policy.JsonRequestPolicy;
import com.dev.weixin.tools.utils.policy.RequestPolicy;

/**
 * @author 飘渺青衣
 * @see
 */
@Service("weixinMenuService")
public class MenuServiceImpl implements MenuService {

	private BaseService baseService;
	
	@Autowired
	public MenuServiceImpl(BaseService baseService) {
		this.baseService = baseService;
	}
	
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.MenuService#create(com.dev.weixin.tools.models.menu.Menu)
	 */
	@Override
	public boolean create(Menu menu) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		RequestPolicy<ErrorMsg> policy = new JsonRequestPolicy<ErrorMsg>(
				URLConstants.Menu.URL_MENU_CREATE,
				WeixinConfig.ENCODING,
				params, menu, ErrorMsg.class);
		ErrorMsg error = WeixinHttpClient.request(policy);
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.MenuService#remove()
	 */
	@Override
	public boolean remove() throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		RequestPolicy<ErrorMsg> policy = new GetRequestPolicy<ErrorMsg>(
				URLConstants.Menu.URL_MENU_DELETE,
				WeixinConfig.ENCODING,
				params, ErrorMsg.class);
		ErrorMsg error = WeixinHttpClient.request(policy);
		boolean isOk = error != null && StringUtils.equals("0", error.getErrcode());
		if(!isOk) {
			throw new ErrorMsgException(error);
		}
		return isOk;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.MenuService#get()
	 */
	@Override
	public Menu get() throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		RequestPolicy<GetMenu> policy = new GetRequestPolicy<GetMenu>(
				URLConstants.Menu.URL_MENU_GET,
				WeixinConfig.ENCODING,
				params, GetMenu.class);
		return WeixinHttpClient.request(policy).getMenu();
	}

}
