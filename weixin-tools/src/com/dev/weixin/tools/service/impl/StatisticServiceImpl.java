package com.dev.weixin.tools.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.statistic.ArticleSummary;
import com.dev.weixin.tools.models.statistic.ArticleSummaryList;
import com.dev.weixin.tools.models.statistic.ArticleTotal;
import com.dev.weixin.tools.models.statistic.ArticleTotalList;
import com.dev.weixin.tools.models.statistic.Decrease;
import com.dev.weixin.tools.models.statistic.DecreaseList;
import com.dev.weixin.tools.models.statistic.GrandTotal;
import com.dev.weixin.tools.models.statistic.GrandTotalList;
import com.dev.weixin.tools.models.statistic.InterfaceSummary;
import com.dev.weixin.tools.models.statistic.InterfaceSummaryList;
import com.dev.weixin.tools.models.statistic.UpStreamMsg;
import com.dev.weixin.tools.models.statistic.UpStreamMsgDist;
import com.dev.weixin.tools.models.statistic.UpStreamMsgDistList;
import com.dev.weixin.tools.models.statistic.UpStreamMsgList;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.service.StatisticService;
import com.dev.weixin.tools.utils.URLConstants;
import com.dev.weixin.tools.utils.WeixinHttpClient;
import com.dev.weixin.tools.utils.policy.JsonRequestPolicy;
import com.dev.weixin.tools.utils.policy.RequestPolicy;

/**
 * @author 飘渺青衣
 * @see
 */
@Service("weixinStatisticService")
public class StatisticServiceImpl implements StatisticService {

	private BaseService baseService;
	
	@Autowired
	public StatisticServiceImpl(BaseService baseService) {
		this.baseService = baseService;
	}
	
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserSummary(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Decrease> getUserSummary(String begin_date, String end_date)
			throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<DecreaseList> policy = new JsonRequestPolicy<DecreaseList>(
				URLConstants.Statistic.URL_STATISTIC_GETUSERSUMMARY,
				WeixinConfig.ENCODING,
				params, body, DecreaseList.class);
		DecreaseList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserCumulate(java.lang.String, java.lang.String)
	 */
	@Override
	public List<GrandTotal> getUserCumulate(String begin_date, String end_date)
			throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<GrandTotalList> policy = new JsonRequestPolicy<GrandTotalList>(
				URLConstants.Statistic.URL_STATISTIC_GETUSERCUMULATE,
				WeixinConfig.ENCODING,
				params, body, GrandTotalList.class);
		GrandTotalList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}

	public List<ArticleSummary> getArticleSummary(String url, String begin_date,
			String end_date) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<ArticleSummaryList> policy = new JsonRequestPolicy<ArticleSummaryList>(
				url, WeixinConfig.ENCODING,
				params, body, ArticleSummaryList.class);
		ArticleSummaryList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getArticleSummary(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleSummary> getArticleSummary(String begin_date,
			String end_date) throws ErrorMsgException {
		return getArticleSummary(URLConstants.Statistic.URL_STATISTIC_GETARTICLESUMMARY, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getArticleTotal(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleTotal> getArticleTotal(String begin_date, String end_date)
			throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<ArticleTotalList> policy = new JsonRequestPolicy<ArticleTotalList>(
				URLConstants.Statistic.URL_STATISTIC_GETARTICLETOTAL, 
				WeixinConfig.ENCODING,
				params, body, ArticleTotalList.class);
		ArticleTotalList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserRead(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleSummary> getUserRead(String begin_date, String end_date)
			throws ErrorMsgException {
		return getArticleSummary(URLConstants.Statistic.URL_STATISTIC_GETUSERREAD, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserReadHour(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleSummary> getUserReadHour(String begin_date,
			String end_date) throws ErrorMsgException {
		return getArticleSummary(URLConstants.Statistic.URL_STATISTIC_GETUSERREADHOUR, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserShare(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleSummary> getUserShare(String begin_date, String end_date)
			throws ErrorMsgException {
		return getArticleSummary(URLConstants.Statistic.URL_STATISTIC_GETUSERSHARE, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUserShareHour(java.lang.String, java.lang.String)
	 */
	@Override
	public List<ArticleSummary> getUserShareHour(String begin_date,
			String end_date) throws ErrorMsgException {
		return getArticleSummary(URLConstants.Statistic.URL_STATISTIC_GETUSERSHAREHOUR, begin_date, end_date);
	}

	public List<UpStreamMsg> getUpStreamMsg(String url, String begin_date, String end_date)
			throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<UpStreamMsgList> policy = new JsonRequestPolicy<UpStreamMsgList>(
				url, WeixinConfig.ENCODING,
				params, body, UpStreamMsgList.class);
		UpStreamMsgList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}
	
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsg(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsg> getUpStreamMsg(String begin_date, String end_date)
			throws ErrorMsgException {
		return this.getUpStreamMsg(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSG, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgHour(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsg> getUpStreamMsgHour(String begin_date,
			String end_date) throws ErrorMsgException {
		return this.getUpStreamMsg(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGHOUR, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgWeek(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsg> getUpStreamMsgWeek(String begin_date,
			String end_date) throws ErrorMsgException {
		return this.getUpStreamMsg(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGWEEK, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgMonth(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsg> getUpStreamMsgMonth(String begin_date,
			String end_date) throws ErrorMsgException {
		return this.getUpStreamMsg(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGMONTH, begin_date, end_date);
	}

	public List<UpStreamMsgDist> getUpStreamMsgDist(String url, String begin_date,
			String end_date) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<UpStreamMsgDistList> policy = new JsonRequestPolicy<UpStreamMsgDistList>(
				url, WeixinConfig.ENCODING,
				params, body, UpStreamMsgDistList.class);
		UpStreamMsgDistList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgDist(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsgDist> getUpStreamMsgDist(String begin_date,
			String end_date) throws ErrorMsgException {
		return getUpStreamMsgDist(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGDIST, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgDistWeek(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsgDist> getUpStreamMsgDistWeek(String begin_date,
			String end_date) throws ErrorMsgException {
		return getUpStreamMsgDist(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGDISTWEEK, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getUpStreamMsgDistMonth(java.lang.String, java.lang.String)
	 */
	@Override
	public List<UpStreamMsgDist> getUpStreamMsgDistMonth(String begin_date,
			String end_date) throws ErrorMsgException {
		return getUpStreamMsgDist(URLConstants.Statistic.URL_STATISTIC_GETUPSTREAMMSGDISTMONTH, begin_date, end_date);
	}

	public List<InterfaceSummary> getInterfaceSummary(
			String url, String begin_date, String end_date) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new HashMap<String, String>();
		body.put("begin_date", begin_date);
		body.put("end_date", end_date);
		RequestPolicy<InterfaceSummaryList> policy = new JsonRequestPolicy<InterfaceSummaryList>(
				url, WeixinConfig.ENCODING,
				params, body, InterfaceSummaryList.class);
		InterfaceSummaryList result =  WeixinHttpClient.request(policy);
		return result != null ? result.getList() : null;
	}
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getInterfaceSummary(java.lang.String, java.lang.String)
	 */
	@Override
	public List<InterfaceSummary> getInterfaceSummary(String begin_date,
			String end_date) throws ErrorMsgException {
		return getInterfaceSummary(URLConstants.Statistic.URL_STATISTIC_GETINTERFACESUMMARY, begin_date, end_date);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.StatisticService#getInterfaceSummaryHour(java.lang.String, java.lang.String)
	 */
	@Override
	public List<InterfaceSummary> getInterfaceSummaryHour(String begin_date,
			String end_date) throws ErrorMsgException {
		return getInterfaceSummary(URLConstants.Statistic.URL_STATISTIC_GETINTERFACESUMMARYHOUR, begin_date, end_date);
	}

}
