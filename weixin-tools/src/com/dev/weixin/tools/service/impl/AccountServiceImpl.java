package com.dev.weixin.tools.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.weixin.tools.models.WeixinConfig;
import com.dev.weixin.tools.models.account.ShortUrl;
import com.dev.weixin.tools.models.account.Ticket;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.service.AccountService;
import com.dev.weixin.tools.service.BaseService;
import com.dev.weixin.tools.utils.URLConstants;
import com.dev.weixin.tools.utils.WeixinHttpClient;
import com.dev.weixin.tools.utils.policy.JsonRequestPolicy;
import com.dev.weixin.tools.utils.policy.RequestPolicy;

/**
 * @author 飘渺青衣
 * @see
 */
@Service("weixinAccountService")
public class AccountServiceImpl implements AccountService {
	
	private BaseService baseService;
	
	@Autowired
	public AccountServiceImpl(BaseService baseService) {
		this.baseService = baseService;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.AccountService#createTempTicket(int, java.lang.String)
	 */
	@Override
	public Ticket createTempTicket(int expire_seconds, int scene_id) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, Object> body = new LinkedHashMap<String, Object>();
		body.put("expire_seconds", expire_seconds);
		body.put("action_name", "QR_SCENE");
		Map<String, Object> action_info = new HashMap<String, Object>();
		body.put("action_info", action_info);
		Map<String, Object> scene = new HashMap<String, Object>();
		scene.put("scene_id", scene_id);
		action_info.put("scene", scene);
		RequestPolicy<Ticket> policy = new JsonRequestPolicy<Ticket>(
				URLConstants.Account.URL_ACCOUNT_CREATE,
				WeixinConfig.ENCODING,
				params, body, Ticket.class);
		return WeixinHttpClient.request(policy);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.AccountService#createTicket(java.lang.String)
	 */
	@Override
	public Ticket createTicket(String scene_str) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, Object> body = new LinkedHashMap<String, Object>();
		body.put("action_name", "QR_LIMIT_STR_SCENE");
		Map<String, Object> action_info = new HashMap<String, Object>();
		body.put("action_info", action_info);
		Map<String, Object> scene = new HashMap<String, Object>();
		scene.put("scene_str", scene_str);
		action_info.put("scene", scene);
		RequestPolicy<Ticket> policy = new JsonRequestPolicy<Ticket>(
				URLConstants.Account.URL_ACCOUNT_CREATE,
				WeixinConfig.ENCODING,
				params, body, Ticket.class);
		return WeixinHttpClient.request(policy);
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.AccountService#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String ticket) throws UnsupportedEncodingException {
		StringBuilder s = new StringBuilder();
		s.append(URLConstants.Account.URL_ACCOUNT_SHOWQRCODE);
		s.append("?ticket=");
		s.append(URLEncoder.encode(ticket, WeixinConfig.ENCODING));
		return s.toString();
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.service.AccountService#getShortUrl(java.lang.String)
	 */
	@Override
	public String getShortUrl(String long_url) throws ErrorMsgException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", this.baseService.getAccessToken());
		Map<String, String> body = new LinkedHashMap<String, String>();
		body.put("action", "long2short");
		body.put("long_url", long_url);
		RequestPolicy<ShortUrl> policy = new JsonRequestPolicy<ShortUrl>(
				URLConstants.Account.URL_ACCOUNT_CREATE,
				WeixinConfig.ENCODING,
				params, body, ShortUrl.class);
		return WeixinHttpClient.request(policy).getShort_url();
	}

}
