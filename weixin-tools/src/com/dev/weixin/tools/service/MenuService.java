package com.dev.weixin.tools.service;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.menu.Menu;

/**
 * 菜单接口操作
 * @author 飘渺青衣
 * @see
 */
public interface MenuService {

	/**
	 * 创建菜单
	 * @author 飘渺青衣
	 * @version
	 * @param menu
	 * @return
	 */
	public boolean create(Menu menu) throws ErrorMsgException;
	
	/**
	 * 菜单删除
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public boolean remove() throws ErrorMsgException;
	
	/**
	 * 菜单获取
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public Menu get() throws ErrorMsgException;
}
