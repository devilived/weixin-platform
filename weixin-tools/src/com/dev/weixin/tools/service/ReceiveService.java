package com.dev.weixin.tools.service;

import java.io.IOException;
import java.io.InputStream;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.receive.ValidData;
import com.dev.weixin.tools.service.common.ImageHandler;
import com.dev.weixin.tools.service.common.LinkHandler;
import com.dev.weixin.tools.service.common.LocationHandler;
import com.dev.weixin.tools.service.common.ShortVideoHandler;
import com.dev.weixin.tools.service.common.TextHandler;
import com.dev.weixin.tools.service.common.VideoHandler;
import com.dev.weixin.tools.service.common.VoiceHandler;
import com.dev.weixin.tools.service.event.ClickHandler;
import com.dev.weixin.tools.service.event.LocationEventHandler;
import com.dev.weixin.tools.service.event.LocationSelectHandler;
import com.dev.weixin.tools.service.event.PicPhotoOrAlbumHandler;
import com.dev.weixin.tools.service.event.PicSysphotoHandler;
import com.dev.weixin.tools.service.event.PicWeixinHandler;
import com.dev.weixin.tools.service.event.ScanHandler;
import com.dev.weixin.tools.service.event.ScancodePushHandler;
import com.dev.weixin.tools.service.event.ScancodeWaitmsgHandler;
import com.dev.weixin.tools.service.event.SubscribeHandler;
import com.dev.weixin.tools.service.event.UnsubscribeHandler;
import com.dev.weixin.tools.service.event.ViewHandler;
import com.dev.weixin.tools.utils.sha1.AesException;

/**
 * 接收消息接口
 * @author 飘渺青衣
 * @see
 */
public interface ReceiveService {

	/**
	 * 验证消息真实性（单个WeixinConfig时候使用）
	 * @author 飘渺青衣
	 * @version
	 * @param valid
	 * @return
	 * @throws ErrorMsgException 
	 */
	public boolean checkSignature(ValidData valid) throws AesException;
	
	/**
	 * 构造Signature（单个WeixinConfig时候使用）
	 * @author 飘渺青衣
	 * @version
	 * @param valid
	 * @return
	 * @throws ErrorMsgException 
	 */
	public String buildSignature(ValidData valid) throws AesException;
	
	/**
	 * 验证消息真实性
	 * @author 飘渺青衣
	 * @version
	 * @param toUserName 原始ID
	 * @param valid
	 * @return
	 * @throws ErrorMsgException 
	 */
	public boolean checkSignature(String toUserName, ValidData valid) throws AesException;
	
	/**
	 * 构造Signature
	 * @author 飘渺青衣
	 * @version
	 * @param toUserName 原始ID
	 * @param valid
	 * @return
	 * @throws ErrorMsgException 
	 */
	public String buildSignature(String toUserName, ValidData valid) throws AesException;
	
	
	/**
	 * 接收信息
	 * @author 飘渺青衣
	 * @version
	 * @param valid 校验数据
	 * @param is 一般是request.getInputStream()
	 * @return
	 * @throws ErrorMsgException 
	 */
	public String receive(ValidData valid, InputStream is) throws AesException, IOException, ErrorMsgException; 
	
	/**
	 * 接收信息
	 * @author 飘渺青衣
	 * @version
	 * @param valid 校验数据
	 * @param xml 信息
	 * @return
	 * @throws AesException
	 * @throws IOException
	 * @throws ErrorMsgException 
	 */
	public String receive(ValidData valid, String xml) throws AesException, IOException, ErrorMsgException; 
	
	/**
	 * 设置全局接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param globalReceiveHandler
	 */
	public void setGlobalReceiveHandler(GlobalReceiveHandler globalReceiveHandler);
	
	/**
	 * 获得全局接收接口
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public GlobalReceiveHandler getGlobalReceiveHandler();
	
	/**
	 * 设置文本信息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 * @param textHandler
	 */
	public void setTextHandler(TextHandler textHandler);
	
	/**
	 * 删除文本信息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeTextHandler();
	
	/**
	 * 设置图片消息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 * @param imageHandler
	 */
	public void setImageHandler(ImageHandler imageHandler);
	
	/**
	 * 删除图片消息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeImageHandler();
	
	/**
	 * 设置语音消息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 * @param voiceHandler
	 */
	public void setVoiceHandler(VoiceHandler voiceHandler);
	
	/**
	 * 删除语音消息接收事件接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeVoiceHandler();
	
	/**
	 * 设置视频消息接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param videoHandler
	 */
	public void setVideoHandler(VideoHandler videoHandler);
	
	/**
	 * 删除视频消息接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeVideoHandler();
	
	/**
	 * 设置小视频消息接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param shortVideoHandler
	 */
	public void setShortVideoHandler(ShortVideoHandler shortVideoHandler);
	
	/**
	 * 删除小视频消息接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeShortVideoHandler();
	
	/**
	 * 设置地理位置消息接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param locationHandler
	 */
	public void setLocationHandler(LocationHandler locationHandler);
	
	/**
	 * 删除地理位置消息接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeLocationHandler();
	
	/**
	 * 设置链接消息接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param linkHandler
	 */
	public void setLinkHandler(LinkHandler linkHandler);
	
	/**
	 * 删除链接消息接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeLinkHandler();
	
	/**
	 * 设置关注、扫描带参数二维码事件接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param subscribeHandler
	 */
	public void setSubscribeHandler(SubscribeHandler subscribeHandler);
	
	/**
	 * 删除关注、扫描带参数二维码事件接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeSubscribeHandler();
	
	/**
	 * 设置取消订阅信息接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param unsubscribeHandler
	 */
	public void setUnsubscribeHandler(UnsubscribeHandler unsubscribeHandler);
	
	/**
	 * 删除取消订阅信息接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeUnsubscribeHandler();
	
	/**
	 * 设置用户已关注时的事件推送接口
	 * @author 飘渺青衣
	 * @version
	 * @param scanHandler
	 */
	public void setScanHandler(ScanHandler scanHandler);
	
	/**
	 * 删除用户已关注时的事件推送接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeScanHandler();
	
	/**
	 * 设置上报地理位置事件接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param locationEventHandler
	 */
	public void setLocationEventHandler(LocationEventHandler locationEventHandler);
	
	/**
	 * 删除上报地理位置事件接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeLocationEventHandler();
	
	/**
	 * 设置点击菜单拉取消息时的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param clickHandler
	 */
	public void setClickHandler(ClickHandler clickHandler);
	
	/**
	 * 删除点击菜单拉取消息时的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeClickHandler();
	
	/**
	 * 设置点击菜单跳转链接时的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param viewHandler
	 */
	public void setViewHandler(ViewHandler viewHandler);
	
	/**
	 * 删除点击菜单跳转链接时的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeViewHandler();
	
	/**
	 * 设置扫码推事件的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param scancodePushHandler
	 */
	public void setScancodePushHandler(ScancodePushHandler scancodePushHandler);
	
	/**
	 * 删除扫码推事件的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeScancodePushHandler();
	
	/**
	 * 设置扫码推事件且弹出“消息接收中”提示框的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param scancodeWaitmsgHandler
	 */
	public void setScancodeWaitmsgHandler(ScancodeWaitmsgHandler scancodeWaitmsgHandler);
	
	/**
	 * 删除扫码推事件且弹出“消息接收中”提示框的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeScancodeWaitmsgHandler();
	
	/**
	 * 设置弹出系统拍照发图的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param picSysphotoHandler
	 */
	public void setPicSysphotoHandler(PicSysphotoHandler picSysphotoHandler);
	
	/**
	 * 删除弹出系统拍照发图的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removePicSysphotoHandler();
	
	/**
	 * 设置弹出拍照或者相册发图的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param picPhotoOrAlbumHandler
	 */
	public void setPicPhotoOrAlbumHandler(PicPhotoOrAlbumHandler picPhotoOrAlbumHandler);
	
	/**
	 * 删除弹出拍照或者相册发图的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removePicPhotoOrAlbumHandler();
	
	/**
	 * 设置弹出微信相册发图器的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param picWeixinHandler
	 */
	public void setPicWeixinHandler(PicWeixinHandler picWeixinHandler);
	
	/**
	 * 删除弹出微信相册发图器的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removePicWeixinHandler();
	
	/**
	 * 设置弹出地理位置选择器的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 * @param locationSelectHandler
	 */
	public void setLocationSelectHandler(LocationSelectHandler locationSelectHandler);
	
	/**
	 * 删除弹出地理位置选择器的事件推送接收接口
	 * @author 飘渺青衣
	 * @version
	 */
	public void removeLocationSelectHandler();
}
