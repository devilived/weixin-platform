package com.dev.weixin.tools.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.user.Group;
import com.dev.weixin.tools.models.user.Oauth2AccessToken;
import com.dev.weixin.tools.models.user.Oauth2UserInfo;
import com.dev.weixin.tools.models.user.UserInfo;
import com.dev.weixin.tools.models.user.UserList;

/**
 * 用户管理微信接口
 * @author 飘渺青衣
 * @see
 */
public interface UserService {

	/**
	 * 创建用户分组
	 * @author 飘渺青衣
	 * @version
	 * @param group
	 * @return
	 * @throws ErrorMsgException
	 */
	public Group createGroup(Group group) throws ErrorMsgException;
	
	/**
	 * 查询所有分组
	 * @author 飘渺青衣
	 * @version
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<Group> getGroups() throws ErrorMsgException;
	
	/**
	 * 查询用户所在分组
	 * @author 飘渺青衣
	 * @version
	 * @param openid
	 * @return
	 * @throws ErrorMsgException
	 */
	public String getGroupId(String openid) throws ErrorMsgException;
	
	/**
	 * 修改分组名
	 * @author 飘渺青衣
	 * @version
	 * @param group
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean updateGroup(Group group) throws ErrorMsgException;
	
	
	/**
	 * 移动用户分组
	 * @author 飘渺青衣
	 * @version
	 * @param openid
	 * @param to_groupid
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean moveUserToGroup(String openid, String to_groupid) throws ErrorMsgException;
	
	
	/**
	 * 批量移动用户分组
	 * @author 飘渺青衣
	 * @version
	 * @param openids
	 * @param to_groupid
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean moveUserToGroup(List<String> openids, String to_groupid) throws ErrorMsgException;
	
	/**
	 * 设置备注名
	 * @author 飘渺青衣
	 * @version
	 * @param openid
	 * @param remark
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean updateUserRemark(String openid, String remark) throws ErrorMsgException;
	
	/**
	 * 获取用户信息
	 * @author 飘渺青衣
	 * @version
	 * @param openid
	 * @param lang zh_CN 简体，zh_TW 繁体，en 英语
	 * @return
	 */
	public UserInfo getUserInfo(String openid, String lang) throws ErrorMsgException;
	
	/**
	 * 获取用户列表
	 * @author 飘渺青衣
	 * @version
	 * @param next_openid
	 * @return
	 * @throws ErrorMsgException
	 */
	public UserList getUserList(String next_openid)  throws ErrorMsgException;
	
	/**
	 * 创建oauth2认证链接
	 * @author 飘渺青衣
	 * @version
	 * @param redirect_uri
	 * @param scope
	 * @param state
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String buildOauth2Url(String redirect_uri, String scope, String state) throws UnsupportedEncodingException;
	
	/**
	 * 通过code换取网页授权access_token
	 * @author 飘渺青衣
	 * @version
	 * @param code
	 * @return
	 * @throws ErrorMsgException 
	 */
	public Oauth2AccessToken getOauth2AccessToken(String code) throws ErrorMsgException;
	
	/**
	 * 刷新access_token（如果需要）
	 * @author 飘渺青衣
	 * @version
	 * @param refresh_token
	 * @return
	 * @throws ErrorMsgException
	 */
	public Oauth2AccessToken refreshOauth2AccessToken(String refresh_token) throws ErrorMsgException;
	
	/**
	 * 拉取用户信息(需scope为 snsapi_userinfo)
	 * @author 飘渺青衣
	 * @version
	 * @param oauth2_access_token 网页授权接口调用凭证,注意：此oauth2_access_token与基础支持的access_token不同
	 * @param openid 用户的唯一标识
	 * @param lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
	 * @return
	 * @throws ErrorMsgException
	 */
	public Oauth2UserInfo getOauth2UserInfo(String oauth2_access_token, String openid, String lang) throws ErrorMsgException;
	
	/**
	 * 检验授权凭证（access_token）是否有效
	 * @author 飘渺青衣
	 * @version
	 * @param oauth2_access_token 网页授权接口调用凭证,注意：此oauth2_access_token与基础支持的access_token不同
	 * @param openid 用户的唯一标识
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean checkOauth2AccessToken(String oauth2_access_token, String openid) throws ErrorMsgException;
	
}
