package com.dev.weixin.tools.service;

import java.io.File;
import java.util.List;

import com.dev.weixin.tools.models.BaseMsg;
import com.dev.weixin.tools.models.customer.Customer;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;

/**
 * 客服微信操作接口
 * @author 飘渺青衣
 * @see
 */
public interface CustomerService {

	/**
	 * 添加客服帐号
	 * @author 飘渺青衣
	 * @version
	 * @param customer
	 */
	public boolean add(Customer customer) throws ErrorMsgException;
	
	/**
	 * 修改客服
	 * @author 飘渺青衣
	 * @version
	 * @param customer
	 */
	public boolean update(Customer customer) throws ErrorMsgException;
	
	/**
	 * 删除客服账户
	 * @author 飘渺青衣
	 * @version
	 * @param customer
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean remove(Customer customer) throws ErrorMsgException;
	
	/**
	 * 获取客服列表
	 * @author 飘渺青衣
	 * @version
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<Customer> getKfList() throws ErrorMsgException;
	
	/**
	 * 客服发送消息
	 * @author 飘渺青衣
	 * @version
	 * @param msg
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean send(BaseMsg msg) throws ErrorMsgException;
	
	/**
	 * 设置客服帐号的头像
	 * @author 飘渺青衣
	 * @version
	 * @param kf_account
	 * @param file
	 * @return
	 * @throws ErrorMsgException
	 */
	public boolean updateHeadImg(String kf_account, File file) throws ErrorMsgException;
}
