package com.dev.weixin.tools.service;

import java.io.UnsupportedEncodingException;

import com.dev.weixin.tools.models.account.Ticket;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;

/**
 * 账号管理
 * @author 飘渺青衣
 * @see
 */
public interface AccountService {

	/**
	 * 创建临时二维码
	 * @author 飘渺青衣
	 * @version
	 * @param expire_seconds
	 * @param scene_id
	 * @return
	 * @throws ErrorMsgException 
	 */
	public Ticket createTempTicket(int expire_seconds, int scene_id) throws ErrorMsgException;
	
	/**
	 * 创建永久二维码
	 * @author 飘渺青衣
	 * @version
	 * @param scene_str 场景值ID（字符串形式的ID），字符串类型，长度限制为1到64，仅永久二维码支持此字段
	 * @return
	 */
	public Ticket createTicket(String scene_str) throws ErrorMsgException;
	
	/**
	 * 获得展示链接
	 * @author 飘渺青衣
	 * @version
	 * @param ticket
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public String getUrl(String ticket) throws UnsupportedEncodingException;
	
	/**
	 * 长链接转短链接接口
	 * @author 飘渺青衣
	 * @version
	 * @param long_url
	 * @return
	 * @throws ErrorMsgException 
	 */
	public String getShortUrl(String long_url) throws ErrorMsgException;
}
