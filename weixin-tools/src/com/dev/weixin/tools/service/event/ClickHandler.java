package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.MenuEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 点击菜单拉取消息时的事件推送接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface ClickHandler extends ReceiveHandler<MenuEvent> {

}
