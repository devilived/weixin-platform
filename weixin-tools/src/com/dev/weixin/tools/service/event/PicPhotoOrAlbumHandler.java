package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.PicSysphotoEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 弹出拍照或者相册发图的事件推送接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface PicPhotoOrAlbumHandler extends ReceiveHandler<PicSysphotoEvent> {

}
