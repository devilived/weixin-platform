package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.SVSCEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 用户已关注时的事件推送接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface ScanHandler extends ReceiveHandler<SVSCEvent> {

}
