package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.SVSCEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 关注、扫描带参数二维码事件接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface SubscribeHandler extends ReceiveHandler<SVSCEvent> {

}
