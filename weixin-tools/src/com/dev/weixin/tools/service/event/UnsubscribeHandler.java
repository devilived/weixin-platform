package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.EventMsg;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 取消订阅信息接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface UnsubscribeHandler extends ReceiveHandler<EventMsg> {

}
