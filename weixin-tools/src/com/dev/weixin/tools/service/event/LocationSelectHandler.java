package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.LocationSelectEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 弹出地理位置选择器的事件推送接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface LocationSelectHandler extends ReceiveHandler<LocationSelectEvent> {

}
