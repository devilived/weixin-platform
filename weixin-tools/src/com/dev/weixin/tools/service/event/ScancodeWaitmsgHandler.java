package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.ScancodePushEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 扫码推事件且弹出“消息接收中”提示框的事件推送接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface ScancodeWaitmsgHandler extends ReceiveHandler<ScancodePushEvent> {

}
