package com.dev.weixin.tools.service.event;

import com.dev.weixin.tools.models.base.Handler;
import com.dev.weixin.tools.models.receive.event.LocationEvent;
import com.dev.weixin.tools.service.ReceiveHandler;

/**
 * 上报地理位置事件接收接口
 * @author 飘渺青衣
 * @see
 */
@Handler
public interface LocationEventHandler extends ReceiveHandler<LocationEvent> {

}
