package com.dev.weixin.tools.service;

import java.util.List;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.dev.weixin.tools.models.statistic.ArticleSummary;
import com.dev.weixin.tools.models.statistic.ArticleTotal;
import com.dev.weixin.tools.models.statistic.Decrease;
import com.dev.weixin.tools.models.statistic.GrandTotal;
import com.dev.weixin.tools.models.statistic.InterfaceSummary;
import com.dev.weixin.tools.models.statistic.UpStreamMsg;
import com.dev.weixin.tools.models.statistic.UpStreamMsgDist;

/**
 * 统计接口
 * @author 飘渺青衣
 * @see
 */
public interface StatisticService {

	/**
	 * 获取用户增减数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<Decrease> getUserSummary(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取累计用户数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<GrandTotal> getUserCumulate(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取图文群发每日数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleSummary> getArticleSummary(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 图文群发总数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleTotal> getArticleTotal(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取图文统计数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleSummary> getUserRead(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取图文统计分时数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleSummary> getUserReadHour(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取图文分享转发数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleSummary> getUserShare(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取图文分享转发分时数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<ArticleSummary> getUserShareHour(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送概况数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsg> getUpStreamMsg(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息分送分时数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsg> getUpStreamMsgHour(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送周数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsg> getUpStreamMsgWeek(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送月数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsg> getUpStreamMsgMonth(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送分布数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsgDist> getUpStreamMsgDist(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送分布周数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsgDist> getUpStreamMsgDistWeek(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取消息发送分布月数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<UpStreamMsgDist> getUpStreamMsgDistMonth(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取接口分析数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<InterfaceSummary> getInterfaceSummary(String begin_date, String end_date) throws ErrorMsgException;
	
	/**
	 * 获取接口分析分时数据
	 * @author 飘渺青衣
	 * @version
	 * @param begin_date
	 * @param end_date
	 * @return
	 * @throws ErrorMsgException
	 */
	public List<InterfaceSummary> getInterfaceSummaryHour(String begin_date, String end_date) throws ErrorMsgException;
}
