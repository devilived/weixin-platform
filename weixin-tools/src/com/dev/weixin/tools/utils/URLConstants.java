package com.dev.weixin.tools.utils;

/**
 * 微信接口地址
 * @author 飘渺青衣
 * @see
 */
public class URLConstants {

	public class Base {
		/**
		 * 获得access token地址
		 */
		public final static String URL_ACCESS_TOKEN = "https://api.weixin.qq.com/cgi-bin/token";
		
		/**
		 * 获取微信服务器IP地址
		 */
		public final static String URL_IP_LIST = "https://api.weixin.qq.com/cgi-bin/getcallbackip";
		
	}
	
	public class Customer {
		/**
		 * 添加客服帐号
		 */
		public final static String URL_KFACCOUNT_ADD = "https://api.weixin.qq.com/customservice/kfaccount/add"; 
		
		/**
		 * 修改客服帐号
		 */
		public final static String URL_KFACCOUNT_UPDATE = "https://api.weixin.qq.com/customservice/kfaccount/update";
		
		/**
		 * 删除客服账户
		 */
		public final static String URL_KFACCOUNT_DEL = "https://api.weixin.qq.com/customservice/kfaccount/del";
		
		/**
		 * 上传客服头像
		 */
		public final static String URL_KFACCOUNT_UPLOADHEADIMG = "https:// api.weixin.qq.com/customservice/kfaccount/uploadheadimg";
		
		/**
		 * 获取客服列表
		 */
		public final static String URL_KFACCOUNT_GETKFLIST = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist";
		
		/**
		 * 客服发送信息
		 */
		public final static String URL_KFACCOUNT_SEND = "https://api.weixin.qq.com/cgi-bin/message/custom/send";
		
	}

	public class Menu {
		
		/**
		 * 菜单创建
		 */
		public final static String URL_MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create";
		
		/**
		 * 菜单获取
		 */
		public final static String URL_MENU_GET = "https://api.weixin.qq.com/cgi-bin/menu/get";
		
		/**
		 * 菜单删除
		 */
		public final static String URL_MENU_DELETE = "https://api.weixin.qq.com/cgi-bin/menu/delete";
	}

	public class User {
		
		/**
		 * 用户分组创建
		 */
		public final static String URL_GROUP_CREATE = "https://api.weixin.qq.com/cgi-bin/groups/create";
		
		/**
		 * 获取用户分组
		 */
		public final static String URL_GROUP_GET = "https://api.weixin.qq.com/cgi-bin/groups/get";
		
		/**
		 * 查询用户所在分组
		 */
		public final static String URL_GROUP_GETID = "https://api.weixin.qq.com/cgi-bin/groups/getid";
		
		/**
		 * 修改分组名
		 */
		public final static String URL_GROUP_UPDATE = "https://api.weixin.qq.com/cgi-bin/groups/update";
		
		/**
		 * 移动用户分组
		 */
		public final static String URL_MEMBERS_UPDATE = "https://api.weixin.qq.com/cgi-bin/groups/members/update";
		
		/**
		 * 批量移动用户分组
		 */
		public final static String URL_MEMBERS_BATCHUPDATE = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate";
		
		/**
		 * 设置备注名
		 */
		public final static String URL_USER_REMARK = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark";
		
		/**
		 * 获取用户基本信息
		 */
		public final static String URL_USER_INFO = "https://api.weixin.qq.com/cgi-bin/user/info";
		
		/**
		 * 获取用户列表
		 */
		public final static String URL_USER_GET = "https://api.weixin.qq.com/cgi-bin/user/get";
		
		/**
		 * OAUTH2认证链接
		 */
		public final static String URL_OAUTH2_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";
		
		/**
		 * 通过code换取网页授权access_token
		 */
		public final static String URL_OAUTH2_ACCESSTOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token";
		
		/**
		 * 刷新access_token（如果需要）
		 */
		public final static String URL_OAUTH2_REFRESHTOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token";
		
		/**
		 * 拉取用户信息(需scope为 snsapi_userinfo)
		 */
		public final static String URL_OAUTH2_SNSUSERINFO = "https://api.weixin.qq.com/sns/userinfo";
		
		/**
		 * 检验授权凭证（access_token）是否有效
		 */
		public final static String URL_OAUTH2_ACCESSTOKENCHECK = "https://api.weixin.qq.com/sns/auth";
	}
	
	public class Statistic {
		
		/**
		 * 获取用户增减数据
		 */
		public final static String URL_STATISTIC_GETUSERSUMMARY = "https://api.weixin.qq.com/datacube/getusersummary";
		
		/**
		 * 获取累计用户数据
		 */
		public final static String URL_STATISTIC_GETUSERCUMULATE = "https://api.weixin.qq.com/datacube/getusercumulate";
		
		/**
		 * 获取图文群发每日数据
		 */
		public final static String URL_STATISTIC_GETARTICLESUMMARY = "https://api.weixin.qq.com/datacube/getarticlesummary";
		
		/**
		 * 获取图文群发总数据
		 */
		public final static String URL_STATISTIC_GETARTICLETOTAL = "https://api.weixin.qq.com/datacube/getarticletotal";
		
		/**
		 * 获取图文统计数据
		 */
		public final static String URL_STATISTIC_GETUSERREAD = "https://api.weixin.qq.com/datacube/getuserread";
		
		/**
		 * 获取图文统计分时数据
		 */
		public final static String URL_STATISTIC_GETUSERREADHOUR = "https://api.weixin.qq.com/datacube/getuserreadhour";
		
		/**
		 * 获取图文分享转发数据
		 */
		public final static String URL_STATISTIC_GETUSERSHARE = "https://api.weixin.qq.com/datacube/getusershare";
		
		/**
		 * 获取图文分享转发分时数据
		 */
		public final static String URL_STATISTIC_GETUSERSHAREHOUR = "https://api.weixin.qq.com/datacube/getusersharehour";
		
		/**
		 * 获取消息发送概况数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSG = "https://api.weixin.qq.com/datacube/getupstreammsg";
		
		/**
		 * 获取消息分送分时数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGHOUR = "https://api.weixin.qq.com/datacube/getupstreammsghour";
		
		/**
		 * 获取消息发送周数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGWEEK = "https://api.weixin.qq.com/datacube/getupstreammsgweek";
		
		/**
		 * 获取消息发送月数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGMONTH = "https://api.weixin.qq.com/datacube/getupstreammsgmonth";
		
		/**
		 * 获取消息发送分布数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGDIST = "https://api.weixin.qq.com/datacube/getupstreammsgdist";
		
		/**
		 * 获取消息发送分布周数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGDISTWEEK = "https://api.weixin.qq.com/datacube/getupstreammsgdistweek";
		
		/**
		 * 获取消息发送分布月数据
		 */
		public final static String URL_STATISTIC_GETUPSTREAMMSGDISTMONTH = "https://api.weixin.qq.com/datacube/getupstreammsgdistmonth";
		
		/**
		 * 获取接口分析数据
		 */
		public final static String URL_STATISTIC_GETINTERFACESUMMARY = "https://api.weixin.qq.com/datacube/getinterfacesummary";
		
		/**
		 * 获取接口分析分时数据
		 */
		public final static String URL_STATISTIC_GETINTERFACESUMMARYHOUR = "https://api.weixin.qq.com/datacube/getinterfacesummaryhour";
		
	}
	
	public class Material {
		
		/**
		 * 新增临时素材
		 */
		public final static String URL_MATERIAL_ADDTEMPMATERIAL = "https://api.weixin.qq.com/cgi-bin/media/upload";
		
		/**
		 * 获取临时素材
		 */
		public final static String URL_MATERIAL_GETTEMPMATERIAL = "https://api.weixin.qq.com/cgi-bin/media/get";
		
		/**
		 * 新增永久图文素材
		 */
		public final static String URL_MATERIAL_ADDNEWS = "https://api.weixin.qq.com/cgi-bin/material/add_news";
		
		/**
		 * 修改永久图文素材
		 */
		public final static String URL_MATERIAL_UPDATENEWS = "https://api.weixin.qq.com/cgi-bin/material/update_news";
		
		/**
		 * 新增其他类型永久素材
		 */
		public final static String URL_MATERIAL_ADDMATERIAL = "http://api.weixin.qq.com/cgi-bin/material/add_material";
		
		/**
		 * 获取永久素材
		 */
		public final static String URL_MATERIAL_GETMATERIAL = "https://api.weixin.qq.com/cgi-bin/material/get_material";
		
		/**
		 * 删除永久素材
		 */
		public final static String URL_MATERIAL_DELMATERIAL = "https://api.weixin.qq.com/cgi-bin/material/del_material";
		
		/**
		 * 获取素材总数
		 */
		public final static String URL_MATERIAL_GETMATERIALCOUNT = "https://api.weixin.qq.com/cgi-bin/material/get_materialcount";
		
		/**
		 * 获取素材列表
		 */
		public final static String URL_MATERIAL_GETMATERIALLIST = "https://api.weixin.qq.com/cgi-bin/material/batchget_material";
	}
	
	public class Account {
		
		/**
		 * 创建二维码ticket
		 */
		public final static String URL_ACCOUNT_CREATE = "https://api.weixin.qq.com/cgi-bin/qrcode/create";
		
		/**
		 * 通过ticket换取二维码
		 */
		public final static String URL_ACCOUNT_SHOWQRCODE = "https://mp.weixin.qq.com/cgi-bin/showqrcode";
		
		/**
		 * 长链接转短链接接口
		 */
		public final static String URL_ACCOUNT_SHORTURL = "https://api.weixin.qq.com/cgi-bin/shorturl";
	}
}
