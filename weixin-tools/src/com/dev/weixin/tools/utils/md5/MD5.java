package com.dev.weixin.tools.utils.md5;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5加密
 * @author 飘渺青衣
 * @see
 */
public class MD5 {

	/**
	 * MD5 32位加密
	 * @author 飘渺青衣
	 * @version
	 * @param source
	 * @return
	 */
	public static String getEncryption32(String source) {
		String result = null;
		try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
		return result;
	}
	
	/**
	 * MD5 16位加密
	 * @author 飘渺青衣
	 * @version
	 * @param source
	 * @return
	 */
	public static String getEncryption16(String source) {
		String result = getEncryption32(source);
		return result != null ? result.substring(8, 24) : null;
	}
}
