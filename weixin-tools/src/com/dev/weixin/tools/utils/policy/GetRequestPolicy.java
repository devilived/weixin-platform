package com.dev.weixin.tools.utils.policy;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import com.dev.weixin.tools.models.base.ErrorMsg;
import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author 飘渺青衣
 * @see
 */
public class GetRequestPolicy<T> implements RequestPolicy<T> {
	
	private Map<String, String> getParameters;
	
	private Class<T> resultClass;
	
	private String url;
	
	private String encoding;
	
	public GetRequestPolicy(
			String url, String encoding, 
			Map<String, String> getParameters, Class<T> resultClass) {
		this.url = url;
		this.encoding = encoding;
		this.resultClass = resultClass;
		this.getParameters = getParameters;
		if(this.resultClass == null) {
			throw new NullPointerException("resultClass不能为 null！");
		}
		if(this.getParameters == null) {
			this.getParameters = new HashMap<String, String>();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.utils.policy.RequestPolicy#getResultClass()
	 */
	@Override
	public Class<T> getResultClass() {
		return this.resultClass;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.utils.policy.RequestPolicy#getGetParameters()
	 */
	@Override
	public Map<String, String> getGetParameters() {
		return this.getParameters;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.utils.policy.RequestPolicy#getUrl()
	 */
	@Override
	public String getUrl() {
		return this.url;
	}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.utils.policy.RequestPolicy#request(org.apache.http.client.methods.HttpPost, com.fasterxml.jackson.databind.ObjectMapper)
	 */
	@Override
	public void request(HttpPost post, ObjectMapper mapper) {}

	/* (non-Javadoc)
	 * @see com.dev.weixin.tools.utils.policy.RequestPolicy#response(org.apache.http.HttpEntity, com.fasterxml.jackson.databind.ObjectMapper)
	 */
	@Override
	public T response(HttpResponse response, HttpEntity entity, ObjectMapper mapper)
			throws ErrorMsgException {
		String content = null;
		try {
			content = EntityUtils.toString(entity, this.getEncoding());
			LOG.info(content);
	    	return mapper.readValue(content, this.getResultClass());
		} catch (ParseException e) {
			LOG.error(e);
			if(content != null) {
				try {
					ErrorMsg error = mapper.readValue(content, ErrorMsg.class);
					throw new ErrorMsgException(error, content);
				} catch (JsonParseException e1) {
					LOG.error(e1);
				} catch (JsonMappingException e1) {
					LOG.error(e1);
				} catch (IOException e1) {
					LOG.error(e1);
				}
			}
		} catch (IOException e) {
			LOG.error(e);
		}
    	return null;
	}

	public String getEncoding() {
		return encoding;
	}

}
