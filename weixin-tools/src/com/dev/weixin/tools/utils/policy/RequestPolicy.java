package com.dev.weixin.tools.utils.policy;

import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dev.weixin.tools.models.exceptions.ErrorMsgException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 请求策略
 * @author 飘渺青衣
 * @see
 */
public interface RequestPolicy<T> {
	
	public static final Logger LOG = LogManager.getLogger(RequestPolicy.class);
	
	/**
	 * 获得结果类型
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public Class<T> getResultClass();
	
	/**
	 * 获得链接GET请求参数
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public Map<String, String> getGetParameters();
	
	/**
	 * 获得链接
	 * @author 飘渺青衣
	 * @version
	 * @return
	 */
	public String getUrl();
	
	/**
	 * 请求
	 * @author 飘渺青衣
	 * @version
	 * @param post
	 * @param mapper
	 */
	public void request(HttpPost post, ObjectMapper mapper);
	
	/**
	 * 响应
	 * @author 飘渺青衣
	 * @version
	 * @param response
	 * @param entity
	 * @param mapper
	 * @return
	 */
	public T response(HttpResponse response, HttpEntity entity,  ObjectMapper mapper) throws ErrorMsgException;
	
}
